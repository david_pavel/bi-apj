package cz.cvut.pavelda2.eshop.integration.service.impl;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.InMemoryProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class InMemoryDaoFactory extends DaoFactory {
    ProductDao productDao = new InMemoryProductDao();

    @Override
    public ProductDao getProductDao() {
        return productDao;
    }

    @Override
    public CustomerDao getCustomerDao() throws EshopException {
        throw new EshopException("Not implemented in memeory.");
    }

    @Override
    public OrderDao getOrderDao() throws EshopException {
        throw new EshopException("Not implemented in memeory.");
    }
}
