package cz.cvut.pavelda2.eshop.integration.dao.impl;

import java.util.*;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class InMemoryProductDao implements ProductDao {
    private final Map<ProductId,Product> products;
    private Integer idSequence;

    public InMemoryProductDao() {
        this.products = new HashMap<>();
        this.idSequence = 0;
    }

    private synchronized ProductId generateNewId() {
        return new ProductId(idSequence++);
    }

    @Override
    public List<Product> findAll() {
        return new ArrayList<>(products.values());
    }

    @Override
    public Product findById(ProductId entityId) {
        return products.get(entityId);
    }

    @Override
    public Product create(Product entity) {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Can not create product which has assigned id: " + entity.getId());
        }
        Product product = new Product(generateNewId(), entity.getName(), entity.getPrice());
        products.put(product.getId(), product);
        return product;
    }

    @Override
    public Product update(Product entity) {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Can not update product which has not assigned id.");
        }
        products.put(entity.getId(),entity);
        return entity;
    }

    @Override
    public void remove(ProductId productId) {
        products.remove(productId);
    }
}
