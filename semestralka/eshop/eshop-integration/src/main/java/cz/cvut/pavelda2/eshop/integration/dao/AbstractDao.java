package cz.cvut.pavelda2.eshop.integration.dao;

import java.util.List;

import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public interface AbstractDao<T,I> {

    public T create(T entity)  throws EshopException;
    public T update(T entity)  throws EshopException;
    public void remove(I entityId)  throws EshopException;

    public List<T> findAll()  throws EshopException;

    /**
     * Find entity by id.
     * @param entityId id of entity
     * @return If entity by id found returns entity, null otherwise.
     * @throws EshopException in case the SQL processing exception occurs.
     */
    public T findById(I entityId)  throws EshopException;
}
