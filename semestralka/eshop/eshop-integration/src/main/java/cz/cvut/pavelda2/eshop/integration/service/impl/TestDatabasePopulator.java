package cz.cvut.pavelda2.eshop.integration.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import cz.cvut.pavelda2.eshop.integration.service.DatabasePopulator;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class TestDatabasePopulator implements DatabasePopulator {

    private List<Product> products = Collections.unmodifiableList(Arrays.asList(new Product[] {
            new Product("Košík", 129.90),
            new Product("Příbory", 350.0),
            new Product("Šátek", 300.0)
    }));

    @Override
    public void populateDatabase() {
        try {
            populateProducts();
        } catch (EshopException e) {
            e.printStackTrace();
        }
    }

    private void populateProducts() throws EshopException {
        ProductDao productDao = DaoFactory.getInstance().getProductDao();
        for (Product product : products) {
            productDao.create(product);
        }
        DaoFactory.getInstance().getProductDao().create(new Product("Test",25.66));
        System.out.println("Database populated");
    }
}
