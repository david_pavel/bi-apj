package cz.cvut.pavelda2.eshop.integration.service;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.integration.service.impl.InMemoryDaoFactory;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public abstract class DaoFactory {
    private static Logger LOG = Logger.getLogger(DaoFactory.class.getName());

    private static DaoFactory instance = null;

    static ServiceTracker st;

    static public void setSt(ServiceTracker<DaoFactory,DaoFactory> st) {
        DaoFactory.st = st;
    }

    public static DaoFactory getInstance() {
        if (instance == null) {
            instance = (DaoFactory)st.getService();
            if (instance == null) {
                LOG.info("[DaoFactory] Using in memmory implementation.");
                instance = new InMemoryDaoFactory();
            } else {
                LOG.info("[DaoFactory] Using in implementation from service tracker " + instance.getClass().getName());
            }
        }
        return instance;
    }

    public abstract ProductDao getProductDao() throws EshopException;
    public abstract CustomerDao getCustomerDao() throws EshopException;
    public abstract OrderDao getOrderDao() throws EshopException;

}
