package cz.cvut.pavelda2.eshop.integration;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class IntegrationActivator implements BundleActivator {
    private Logger log = Logger.getLogger(IntegrationActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        log.info(">>> Integration module started <<<");
        ServiceTracker<DaoFactory, DaoFactory> st = new ServiceTracker<>(bundleContext, DaoFactory.class, null);
        st.open();
        DaoFactory.setSt(st);
    }


    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Integration module stopped <<<");
    }
}
