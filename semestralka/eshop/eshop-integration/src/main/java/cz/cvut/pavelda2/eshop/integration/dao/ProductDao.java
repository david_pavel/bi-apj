package cz.cvut.pavelda2.eshop.integration.dao;

import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public interface ProductDao extends AbstractDao<Product, ProductId> {
}
