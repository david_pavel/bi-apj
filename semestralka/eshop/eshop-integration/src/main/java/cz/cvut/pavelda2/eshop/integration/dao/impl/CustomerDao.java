package cz.cvut.pavelda2.eshop.integration.dao.impl;

import cz.cvut.pavelda2.eshop.integration.dao.AbstractDao;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.CustomerId;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public interface CustomerDao extends AbstractDao<Customer, CustomerId> {
}
