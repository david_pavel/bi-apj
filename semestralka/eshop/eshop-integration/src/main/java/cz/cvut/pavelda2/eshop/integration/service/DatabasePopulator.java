package cz.cvut.pavelda2.eshop.integration.service;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public interface DatabasePopulator {
    public void populateDatabase();
}
