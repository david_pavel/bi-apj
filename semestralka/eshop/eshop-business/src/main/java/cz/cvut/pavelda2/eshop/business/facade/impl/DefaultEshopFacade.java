package cz.cvut.pavelda2.eshop.business.facade.impl;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import cz.cvut.pavelda2.eshop.model.entity.*;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class DefaultEshopFacade extends EshopFacade {
    private final ProductDao productDao;
    private final CustomerDao customerDao;
    private final OrderDao orderDao;

    public DefaultEshopFacade() throws EshopException {
        productDao = DaoFactory.getInstance().getProductDao();
        customerDao = DaoFactory.getInstance().getCustomerDao();
        orderDao = DaoFactory.getInstance().getOrderDao();
    }

    @Override
    public boolean facadeAvailable() {
        return true;
    }

    // ----- Products ----------------------------------------------------------------------
    @Override
    public Product createProduct(String name, double price) throws EshopException {
        return productDao.create(new Product(name, price));
    }

    @Override
    public Product updateProduct(Integer id, String name, double price) throws EshopException {
        return  productDao.update(new Product(new ProductId(id), name, price));
    }

    @Override
    public void removeProduct(Integer id) throws EshopException {
        productDao.remove(new ProductId(id));
    }

    @Override
    public Product findProductById(Integer id) throws EshopException {
        return productDao.findById(new ProductId(id));
    }

    @Override
    public List<Product> getAllProducts() throws EshopException {
        return productDao.findAll();
    }

    // ----- Customers ----------------------------------------------------------------------
    @Override
    public Customer createCustomer(String name) throws EshopException {
        return customerDao.create(new Customer(name));
    }

    @Override
    public Customer updateCustomer(Integer id, String name) throws EshopException {
        return customerDao.update(new Customer(new CustomerId(id), name));
    }

    @Override
    public void removeCustomer(Integer id) throws EshopException {
        customerDao.remove(new CustomerId(id));
    }

    @Override
    public Customer findCustomerById(Integer id) throws EshopException {
        return customerDao.findById(new CustomerId(id));
    }

    @Override
    public List<Customer> getAllCustomers() throws EshopException {
        return customerDao.findAll();
    }

    // ----- Orders ----------------------------------------------------------------------
    @Override
    public Order createOrder(List<OrderItem> orderItems, Customer customer) throws EshopException {
        return orderDao.create(new Order(orderItems, customer));
    }

    @Override
    public void removeOrder(Integer id) throws EshopException {
        orderDao.remove(new OrderId(id));
    }

    @Override
    public List<Order> getAllOrders() throws EshopException {
        return orderDao.findAll();
    }

}
