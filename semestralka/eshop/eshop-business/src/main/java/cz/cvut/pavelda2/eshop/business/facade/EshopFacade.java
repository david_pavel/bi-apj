package cz.cvut.pavelda2.eshop.business.facade;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.impl.DefaultEshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.*;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public abstract class EshopFacade {
    private static EshopFacade service;
    private static ServiceTracker<EshopFacade, EshopFacade> st;

    public static EshopFacade getService() {
        if (service == null) {
            service = st.getService();
            if (service == null) {
                try {
                    service = new DefaultEshopFacade();
                } catch (EshopException e) {
                    e.printStackTrace();
                }
            }
        }
        return service;
    }

    public static void setServiceTracker(ServiceTracker<EshopFacade, EshopFacade> serviceTracker) {
        EshopFacade.st = serviceTracker;
    }


    // ----- Facade ----------------------------------------------------------------------
    public abstract boolean facadeAvailable();

    // ----- Products ----------------------------------------------------------------------
    public abstract Product createProduct(String name, double price) throws EshopException;
    public abstract Product updateProduct(Integer id, String name, double price) throws EshopException;
    public abstract void removeProduct(Integer id) throws EshopException;
    public abstract Product findProductById(Integer id) throws EshopException;
    public abstract List<Product> getAllProducts() throws EshopException;

    // ----- Customers ----------------------------------------------------------------------
    public abstract Customer createCustomer(String name) throws EshopException;
    public abstract Customer updateCustomer(Integer id, String name) throws EshopException;
    public abstract void removeCustomer(Integer id) throws EshopException;
    public abstract Customer findCustomerById(Integer id) throws EshopException;
    public abstract List<Customer> getAllCustomers() throws EshopException;

    // ----- Orders ----------------------------------------------------------------------
    public abstract Order createOrder(List<OrderItem> orderItems, Customer customer) throws EshopException;
    public abstract void removeOrder(Integer id) throws EshopException;
    public abstract List<Order> getAllOrders() throws EshopException;

}
