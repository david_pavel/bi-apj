package cz.cvut.pavelda2.eshop.business;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class BusinessActivator implements BundleActivator {
    private Logger log = Logger.getLogger(BusinessActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        ServiceTracker<EshopFacade, EshopFacade> st = new ServiceTracker<>(bundleContext, EshopFacade.class, null);
        st.open();
        EshopFacade.setServiceTracker(st);
        log.info(">>> Business module started <<<");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Business module stopped <<<");
    }
}
