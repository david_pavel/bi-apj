package cz.cvut.pavelda2.eshop.business.facade;

import static org.junit.Assert.*;

import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cz.cvut.pavelda2.eshop.model.entity.Product;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class LibraryFacadeTest {

    private EshopFacade facade;

    @Before
    public void before() {
        facade = EshopFacade.getService();
    }

    @Test
    @Ignore
    public void testCreateProduct() throws Exception {
        final String expectedName ="Hřebíky";
        final Double expectedPrice = 1.99;

        Product product = facade.createProduct(expectedName, expectedPrice);

        assertEquals(expectedName, product.getName());
        assertEquals(expectedPrice, product.getPrice());
    }
    @Test
    public void testGetAllProducts() throws Exception {
        final String expectedName ="Hřebíky";
        final Double expectedPrice = 1.99;

        facade.createProduct(expectedName, expectedPrice);
        List<Product> allProducts = facade.getAllProducts();

        assertNotNull(allProducts);
        assertEquals(1, allProducts.size());
        assertNotNull(allProducts.get(0));
        assertEquals(expectedName, allProducts.get(0).getName());
        assertEquals(expectedPrice, allProducts.get(0).getPrice());
    }

}