package cz.cvut.pavelda2.eshop.server.nio.tasks;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.protocol.command.customer.GetAllCustomersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.order.GetAllOrdersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.product.GetAllProductsCommand;
import cz.cvut.pavelda2.eshop.utils.NumberSequence;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public class ClientTask implements Runnable {
    private static final Logger LOG = Logger.getLogger(ClientTask.class.getName());
    private final Socket socket;
    private final int clientNumber;
    private NumberSequence commandSequence;

    private ObjectInputStream ois;
    private ObjectOutputStream oos;

//    Workaround (ClassNotFoundException) - Package representational classes
//    private Class[] _classes = {GetAllProductsCommand.class, GetAllCustomersCommand.class,
//            GetAllOrdersCommand.class, LogoutCommand.class};

    public ClientTask(Socket socket, int clientNumber) throws IOException {
        this.socket = socket;
        this.clientNumber = clientNumber;
        this.commandSequence = new NumberSequence();
        this.ois = new ObjectInputStream(socket.getInputStream());
        this.oos = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        LOG.info(clientNumber + "] Processing of client commands started.");
        startProcessing();
        LOG.info(clientNumber + "] Client logged out.");
    }

    private void startProcessing() {
        try (ObjectInputStream ois = this.ois; ObjectOutputStream oos = this.oos; Socket socket = this.socket) {
            int commandNumber;
            for (; ; ) {
                // Read command
                AbstractCommand command = (AbstractCommand) ois.readObject();
                commandNumber = commandSequence.next();
                LOG.info(MessageFormat.format("[{0}] New command ({1}): {2}", clientNumber, commandNumber, command.toString()));
                if (command instanceof LogoutCommand) {
                    return; // Exit
                }
                // Execute command
                Object result = null;
                try {
                    result = command.execute(EshopFacade.getService());
                } catch (EshopException e) {
                    result = e;
                    e.printStackTrace();
                }
                LOG.info(MessageFormat.format("[{0}] Command response ({1}): {2}", clientNumber, commandNumber, result.toString()));

                // Write response
                oos.writeObject(result);
                oos.flush();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
