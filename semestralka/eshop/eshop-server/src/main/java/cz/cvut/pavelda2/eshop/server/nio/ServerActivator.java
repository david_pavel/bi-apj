package cz.cvut.pavelda2.eshop.server.nio;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.server.nio.tasks.ServerTask;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class ServerActivator implements BundleActivator {
    private static final int DEFAULT_PORT = 1234;
    private Logger log = Logger.getLogger(ServerActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Server module started <<<");

        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new ServerTask(DEFAULT_PORT, executorService));
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Server module stopped <<<");
    }
}
