package cz.cvut.pavelda2.eshop.server.nio.tasks;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.utils.NumberSequence;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public class ServerTask implements Runnable {

    private static final Logger LOG = Logger.getLogger(ServerTask.class.getName());
    private ServerSocket ss;
    private ExecutorService executorService;
    private NumberSequence clientSequence;

    public ServerTask(int port, ExecutorService executorService) throws IOException {
        this.executorService = executorService;
        this.ss = new ServerSocket(port);
        this.clientSequence = new NumberSequence();
    }

    @Override
    public void run() {
        for(;;) {
            try {
                Socket socket = ss.accept();
                LOG.info("New connection accepted.");
                executorService.execute(new ClientTask(socket, clientSequence.next()));
            } catch (IOException e) {
                    e.printStackTrace();
            }
        }
    }
}
