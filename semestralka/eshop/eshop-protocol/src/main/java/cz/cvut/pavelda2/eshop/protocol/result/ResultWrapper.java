package cz.cvut.pavelda2.eshop.protocol.result;

/**
 * Created by David Pavel on 24. 12. 2015.
 */
public class ResultWrapper {
    private Object data;

    public ResultWrapper(Object data) {
        this.data = data;
    }

    public ResultWrapper() {
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
