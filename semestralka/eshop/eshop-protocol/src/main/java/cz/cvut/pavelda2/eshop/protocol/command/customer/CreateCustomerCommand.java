package cz.cvut.pavelda2.eshop.protocol.command.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 11. 1. 2016.
 */
public class CreateCustomerCommand extends AbstractCommand {
    private final String name;

    public CreateCustomerCommand(String name) {
        super();
        this.name = name;
    }

    @Override
    public Object execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.createCustomer(name);
    }
}
