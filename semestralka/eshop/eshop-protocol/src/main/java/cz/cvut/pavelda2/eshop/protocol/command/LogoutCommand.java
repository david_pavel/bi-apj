package cz.cvut.pavelda2.eshop.protocol.command;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public final class LogoutCommand extends AbstractCommand {

    static private LogoutCommand instance = new LogoutCommand();

    public LogoutCommand() {}

    @Override
    public Object execute(EshopFacade eshopFacade) throws EshopException {
        throw new RuntimeException("Logout command should not be executed!");
    }

    public static AbstractCommand getInstance() {
        return instance;
    }
}
