package cz.cvut.pavelda2.eshop.protocol.command.product;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class CreateProductCommand extends AbstractCommand {

    private String name;
    private Double price;

    public CreateProductCommand(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String execute(EshopFacade eshopFacade) throws EshopException {
        eshopFacade.createProduct(name, price);
        return OK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
