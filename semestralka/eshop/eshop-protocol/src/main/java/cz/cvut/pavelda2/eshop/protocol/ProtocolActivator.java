package cz.cvut.pavelda2.eshop.protocol;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class ProtocolActivator implements BundleActivator {
    private Logger log = Logger.getLogger(ProtocolActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Protocol module started <<<");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Protocol module stopped <<<");
    }
}
