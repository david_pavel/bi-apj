package cz.cvut.pavelda2.eshop.protocol.command;

import java.io.Serializable;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public abstract class AbstractCommand implements Serializable {
    static final long serialVersionUID = 1L;

    protected static final String OK = "OK";

    public abstract Object execute(EshopFacade eshopFacade) throws EshopException;

}
