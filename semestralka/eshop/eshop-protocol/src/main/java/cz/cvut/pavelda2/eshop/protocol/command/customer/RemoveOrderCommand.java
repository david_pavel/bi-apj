package cz.cvut.pavelda2.eshop.protocol.command.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 11. 1. 2016.
 */
public class RemoveOrderCommand extends AbstractCommand {
    private final Integer id;

    public RemoveOrderCommand(Integer id) {
        super();
        this.id = id;
    }

    @Override
    public Object execute(EshopFacade eshopFacade) throws EshopException {
        eshopFacade.removeOrder(id);
        return OK;
    }
}
