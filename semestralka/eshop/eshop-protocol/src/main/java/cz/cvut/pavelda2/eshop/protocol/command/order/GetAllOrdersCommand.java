package cz.cvut.pavelda2.eshop.protocol.command.order;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Order;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class GetAllOrdersCommand extends AbstractCommand {

    @Override
    public List<Order> execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.getAllOrders();
    }

}
