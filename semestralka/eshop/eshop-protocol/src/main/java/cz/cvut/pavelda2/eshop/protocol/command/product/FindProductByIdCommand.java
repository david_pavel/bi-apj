package cz.cvut.pavelda2.eshop.protocol.command.product;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class FindProductByIdCommand extends AbstractCommand {

    private Integer id;

    public FindProductByIdCommand(Integer id) {
        this.id = id;
    }

    private FindProductByIdCommand() {}

    @Override
    public Product execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.findProductById(id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
