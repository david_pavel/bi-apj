package cz.cvut.pavelda2.eshop.protocol.command.product;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class GetAllProductsCommand extends AbstractCommand {

    @Override
    public List<Product> execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.getAllProducts();
    }

}
