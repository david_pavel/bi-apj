package cz.cvut.pavelda2.eshop.protocol.command.customer;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.OrderItem;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 11. 1. 2016.
 */
public class CreateOrderCommand extends AbstractCommand {
    private final List<OrderItem> orderItems;
    private final Customer customer;

    public CreateOrderCommand(List<OrderItem> orderItems, Customer customer) {
        super();
        this.orderItems = orderItems;
        this.customer = customer;
    }

    @Override
    public Object execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.createOrder(orderItems, customer);
    }
}
