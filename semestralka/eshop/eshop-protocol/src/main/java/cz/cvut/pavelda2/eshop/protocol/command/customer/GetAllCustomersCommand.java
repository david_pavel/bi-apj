package cz.cvut.pavelda2.eshop.protocol.command.customer;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class GetAllCustomersCommand extends AbstractCommand {

    @Override
    public List<Customer> execute(EshopFacade eshopFacade) throws EshopException {
        return eshopFacade.getAllCustomers();
    }

}
