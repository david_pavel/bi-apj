package cz.cvut.pavelda2.eshop.connection.json;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class JsonConnectionActivator implements BundleActivator {
    private Logger log = Logger.getLogger(JsonConnectionActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> JSON Connection module started <<<");
        bundleContext.registerService(AbstractConnection.class.getName(), new JsonConnection(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> JSON Connection module stopped <<<");
    }
}
