package cz.cvut.pavelda2.eshop.connection.json;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import cz.cvut.pavelda2.eshop.connection.DefaultConnection;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.protocol.result.ResultWrapper;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class JsonConnection extends AbstractConnection {
    protected static final Logger LOG = Logger.getLogger(DefaultConnection.class.getName());
    protected static final int SOCKET_TIMEOUT_MS = 3000;

    protected DataOutputStream oos;
    protected DataInputStream ois;
    protected Socket socket;

    JSONSerializer ser;
    JSONDeserializer<ResultWrapper> des;

    @Override
    public void connect(String host, int port) throws EshopException {
        try {
            LOG.info("Creating connection to " + host + ":" + port);
            socket = new Socket(host, port);
            socket.setSoTimeout(SOCKET_TIMEOUT_MS);
            LOG.info("Init input stream");
            oos = new DataOutputStream(socket.getOutputStream());
            LOG.info("Init output stream");
            ois = new DataInputStream(socket.getInputStream());

            // Set FlexJson classloader
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
            ser = new JSONSerializer();
            des = new JSONDeserializer<>();
        } catch (IOException e) {
            throw new EshopException(e); // Business error
        }
    }

    @Override
    public void disconnect() {
        LOG.info("Disconnecting from server...");
        try (
                DataOutputStream oos = this.oos;
                DataInputStream ois = this.ois;
                Socket s = this.socket) {
            send(LogoutCommand.getInstance());
        } catch (IOException | EshopException e) {
        } finally {
            socket = null;
        }
    }

    @Override
    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }


    public <T> T send(AbstractCommand command) throws EshopException {

        LOG.info("JSON Sending command: " + command.toString());
        if (!isConnected()) {
            throw new EshopException("Can not send command " + command.getClass().getSimpleName() + ". Not connected to a Server.");
        }
        try {
            // Write command
            String json = ser.deepSerialize(command);
            LOG.info("Request: " + json);
            oos.writeUTF(json);
            oos.flush();

            // Read response
            String responseJson = ois.readUTF();
            LOG.info("Reponse: " + responseJson);
            ResultWrapper result = des.deserialize(responseJson);
            if (result.getData() instanceof EshopException) {
                throw (EshopException) result.getData();
            }
            return (T) result.getData();

        } catch (IOException e) {
            throw new EshopException(e); // Business error
        }
    }
}
