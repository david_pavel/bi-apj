package test;

import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * Created by David Pavel on 24. 12. 2015.
 */
public class Test {

    @org.junit.Test
    public void test() {
        JSONSerializer ser = new JSONSerializer()
                .exclude("*.class", "description");


        Product result = new Product(new ProductId(123), "xxx", 123.0);
        String s = ser.deepSerialize(result);

        System.out.println(s);

        JSONDeserializer<Object> der = new JSONDeserializer<>();
        Object res = der.deserialize(s);

        System.out.println(res);
    }
}
