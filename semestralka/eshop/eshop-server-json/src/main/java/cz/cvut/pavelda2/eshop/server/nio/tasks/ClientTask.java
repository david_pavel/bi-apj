package cz.cvut.pavelda2.eshop.server.nio.tasks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.protocol.command.customer.GetAllCustomersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.order.GetAllOrdersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.product.GetAllProductsCommand;
import cz.cvut.pavelda2.eshop.protocol.result.ResultWrapper;
import cz.cvut.pavelda2.eshop.utils.NumberSequence;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public class ClientTask implements Runnable {
    private static final Logger LOG = Logger.getLogger(ClientTask.class.getName());
    private final Socket socket;
    private final int clientNumber;
    private NumberSequence commandSequence;
    JSONSerializer ser;
    JSONDeserializer<AbstractCommand> des;
    private int commandNumber;
    private DataInputStream ois;
    private DataOutputStream oos;

    public ClientTask(Socket socket, int clientNumber) throws IOException {
        this.socket = socket;
        this.clientNumber = clientNumber;
        this.commandSequence = new NumberSequence();
        this.ois = new DataInputStream(socket.getInputStream());
        this.oos = new DataOutputStream(socket.getOutputStream());

        // Set FlexJson classloader
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        this.ser = new JSONSerializer();
        this.des = new JSONDeserializer<>();
    }

    @Override
    public void run() {
        LOG.info(clientNumber + "] Processing of client commands started.");
        startProcessing();
        LOG.info(clientNumber + "] Client logged out.");
    }

    private void startProcessing() {
        try (DataInputStream ois = this.ois; DataOutputStream oos = this.oos; Socket socket = this.socket) {

            for (; ; ) {
                // Read command
                Object command = readCommand();

                // Execute command
                if (command instanceof LogoutCommand) {
                    return; // Exit
                }
                Object result;
                try {
                    result = ((AbstractCommand) command).execute(EshopFacade.getService());
                } catch (EshopException e) {
                    result = e;
                    e.printStackTrace();
                }
                writeResult(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeResult(final Object data) throws IOException {
        String json = ser.deepSerialize(new ResultWrapper(data));
        LOG.info(MessageFormat.format("[{0}] Command response ({1}): {2}", clientNumber, commandNumber, json));
        oos.writeUTF(json);
        oos.flush();
    }

    private Object readCommand() throws IOException {
        String json = ois.readUTF();
        LOG.info(MessageFormat.format("[{0}] New command ({1}): {2}", clientNumber, commandNumber, json));
        AbstractCommand command = des.deserialize(json, AbstractCommand.class);
        commandNumber = commandSequence.next();
        return command;
    }
}
