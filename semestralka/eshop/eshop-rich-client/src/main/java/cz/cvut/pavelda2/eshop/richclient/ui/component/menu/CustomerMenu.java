package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.CreateCustomerAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.DeleteCustomerAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.UpdateCustomerAction;
import javafx.scene.control.Menu;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CustomerMenu extends Menu {


    public CustomerMenu() {
        super(Messages.MENU_CUSTOMERS.createMsg());
        getItems().addAll(
                CreateCustomerAction.instance.createMenuItem(),
                UpdateCustomerAction.instance.createMenuItem(),
                DeleteCustomerAction.instance.createMenuItem()
        );
    }
}
