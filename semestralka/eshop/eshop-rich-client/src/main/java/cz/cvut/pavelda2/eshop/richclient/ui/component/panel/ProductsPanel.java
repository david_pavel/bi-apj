package cz.cvut.pavelda2.eshop.richclient.ui.component.panel;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.ProductObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.CreateProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.DeleteProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.UpdateProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.table.EshopTableView;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Created by David Pavel on 3. 11. 2015.
 */
public class ProductsPanel extends TitledPane implements Observer {
    private static ProductsPanel instance;
    public static ProductsPanel getInstance() {
        if (instance == null) {
            instance = new ProductsPanel();
        }
        return  instance;
    }

    // Dynamic reloding structure
    protected ObservableList<Product> products = FXCollections.observableArrayList();

    private final TableView<Product> table;

    private ProductsPanel() {
        super(Messages.PRODUCTS_LIST.createMsg(), null);
        setContent(table = createProductTable());
        refresh();
        ProductObservable.instance.addObserver(this);
    }

    public void refresh() {
        if (EshopFacade.getService().facadeAvailable()) {
            try {
                Collection<Product> allProducts = EshopFacade.getService().getAllProducts();
                products.clear();
                products.addAll(allProducts);
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
    }

    protected TableView<Product> createProductTable() {
        TableView<Product> table = new EshopTableView<Product>() {
            @Override
            public void onUpdateRowEvent() {
                UpdateProductAction.instance.execute();
            }
            @Override
            public void onDeleteRowEvent() {
                DeleteProductAction.instance.execute();
            }
            @Override
            public void onInsertRowEvent() {
                CreateProductAction.instance.execute();
            }
        };
        TableColumn<Product, ProductId> idCol = new TableColumn(Messages.ID.createMsg());
        idCol.setCellValueFactory(new PropertyValueFactory<Product, ProductId>("id"));
        TableColumn<Product, ProductId> nameCol = new TableColumn(Messages.PRODUCT_NAME.createMsg());
        nameCol.setCellValueFactory(new PropertyValueFactory<Product, ProductId>("name"));
        TableColumn<Product, ProductId> priceCol = new TableColumn(Messages.PRODUCT_PRICE.createMsg());
        priceCol.setCellValueFactory(new PropertyValueFactory<Product, ProductId>("price"));
        table.getColumns().addAll(idCol, nameCol, priceCol);
        table.setItems(products);
        table.setContextMenu(new ContextMenu(
                UpdateProductAction.instance.createMenuItem(),
                DeleteProductAction.instance.createMenuItem()
        ));
        return table;
    }

    @Override
    public void update(Observable o, Object arg) {
        refresh();
    }

    public TableView<Product> getTable() {
        return table;
    }
}
