package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.richclient.ui.component.EshopActionComponent;
import javafx.scene.control.Button;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class EshopButton extends Button implements EshopActionComponent {
    public EshopButton(String name) {
        super(name);
    }
}
