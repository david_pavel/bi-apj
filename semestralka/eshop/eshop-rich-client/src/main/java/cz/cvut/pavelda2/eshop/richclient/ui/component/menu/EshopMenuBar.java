package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import javafx.scene.control.MenuBar;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class EshopMenuBar extends MenuBar {
    public EshopMenuBar() {
        getMenus().addAll(
                new FileMenu(),
                new ProductMenu(),
                new CustomerMenu(),
                new OrderMenu()
        );
    }
}
