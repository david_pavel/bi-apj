package cz.cvut.pavelda2.eshop.richclient.ui.action.product;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.product.CreateProductDialog;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateProductAction extends AbstractAction {

    public static final AbstractAction instance = new CreateProductAction();

    public CreateProductAction() {
        super(Messages.PRODUCT_ADD.createMsg(), KeyCombination.keyCombination("F4"));
    }

    @Override
    public void execute() {
        AbstractCreateDialog createProductDialog = new CreateProductDialog();
        createProductDialog.execute();
    }

    @Override
    public boolean isDisabled() {
        return !EshopFacade.getService().facadeAvailable();
    }
}
