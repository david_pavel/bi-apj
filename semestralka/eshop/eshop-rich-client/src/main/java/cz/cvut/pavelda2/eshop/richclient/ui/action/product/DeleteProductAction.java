package cz.cvut.pavelda2.eshop.richclient.ui.action.product;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.ProductObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.ProductsPanel;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.ObservableList;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class DeleteProductAction extends AbstractAction {

    public static final AbstractAction instance = new DeleteProductAction();

    public DeleteProductAction() {
        super(Messages.PRODUCT_DELETE.createMsg(), KeyCombination.keyCombination("F8"));
    }

    @Override
    public boolean isDisabled() {
        return !EshopFacade.getService().facadeAvailable() || ProductsPanel.getInstance().getTable().getSelectionModel().getSelectedItems().isEmpty();
    }

    @Override
    public void execute() {
        ObservableList<Product> selectedItems = ProductsPanel.getInstance().getTable().getSelectionModel().getSelectedItems();
        for (Product selectedItem : selectedItems) {
            try {
                EshopFacade.getService().removeProduct(selectedItem.getId().getId());
                ProductObservable.instance.markChanged();
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
        ProductObservable.instance.notifyObservers();
    }
}
