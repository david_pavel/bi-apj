package cz.cvut.pavelda2.eshop.richclient.ui.component.window;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.component.menu.EshopMenuBar;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.CustomerPanel;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.OrderPanel;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.ProductsPanel;
import cz.cvut.pavelda2.eshop.richclient.ui.component.toolbar.EshopToolBar;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class MainWindow extends Stage {

    public static MainWindow instance = new MainWindow();

    private final MenuBar eshopMenuBar = new EshopMenuBar();

    private final ToolBar eshopToolBar = new EshopToolBar();

    private BundleContext bundleContext;

    public MainWindow() {
        setupTitleBar();
        setOnCloseRequest(event -> stop());
        VBox rootBox = new VBox(eshopMenuBar, eshopToolBar, new SplitPane(
                ProductsPanel.getInstance(),
                CustomerPanel.getInstance()),
                OrderPanel.getInstance());
        Scene scene = new Scene(rootBox, 800, 600);
        this.setScene(scene);
        this.centerOnScreen();
        this.show();
    }

    private void setupTitleBar() {
        setTitle(Messages.APPLICATION_NAME.createMsg());
        // Set app icon
        InputStream imageStream = getClass().getClassLoader().getResourceAsStream("images/eshop-icon.png");
        if (imageStream!= null) {
            getIcons().add(new Image(imageStream));
        }

    }

    public void stop() {
        Framework f = (Framework) MainWindow.this.getBundleContext().getBundle(0);
        try {
            f.stop();
            f.waitForStop(2000);
        } catch (BundleException | InterruptedException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public BundleContext getBundleContext() {
        return bundleContext;
    }

    public void setBundleContext(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    public MenuBar getMenuBar() {
        return eshopMenuBar;
    }

    public ToolBar getToolBar() {
        return eshopToolBar;
    }
}
