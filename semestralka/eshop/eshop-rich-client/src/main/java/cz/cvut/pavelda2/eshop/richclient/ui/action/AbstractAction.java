package cz.cvut.pavelda2.eshop.richclient.ui.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import cz.cvut.pavelda2.eshop.richclient.ui.component.EshopActionComponent;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.menu.EshopButton;
import cz.cvut.pavelda2.eshop.richclient.ui.component.menu.EshopMenuItem;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public abstract class AbstractAction implements Observer {

    protected final String name;
    protected final KeyCombination keyShortcut;

    Collection<EshopActionComponent> actionComponents = new ArrayList<>();


    public AbstractAction(String name) {
        this(name, KeyCombination.NO_MATCH);
    }

    public AbstractAction(String name, KeyCombination keyShortcut) {
        this.name = name;
        this.keyShortcut = keyShortcut;
        ActionState.instance.registerAction(this);
    }


    protected void _execute() {
        if(isDisabled()) {
            EshopAlert.error("{0} is disabled at the moment.", name);
        } else {
            execute();
        }
    }

    public abstract void execute();

    public MenuItem createMenuItem() {
        EshopMenuItem menuItem = new EshopMenuItem(name);
        actionComponents.add(menuItem);
        menuItem.setOnAction((event) -> _execute());
        menuItem.setAccelerator(keyShortcut);
        return menuItem;
    }

    public Button createButton() {
        EshopButton menuItem = new EshopButton(name);
        actionComponents.add(menuItem);
        menuItem.setOnAction((event) -> _execute());
        return menuItem;
    }

    @Override
    public void update(Observable o, Object arg) {
        checkDisable();
    }

    public abstract boolean isDisabled();

    public void checkDisable() {
        disable(isDisabled());
    }

    protected void disable(boolean disable) {
        for (EshopActionComponent component : actionComponents) {
            component.setDisable(disable);
        }
    }

}
