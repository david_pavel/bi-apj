package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.CustomerObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateCustomerDialog extends AbstractCreateDialog {

    private TextField name;

    public CreateCustomerDialog() {
        setTitle(Messages.CUSTOMER_CREATE_DIALOG_TITLE.createMsg());
        this.initModality(Modality.APPLICATION_MODAL);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_CREATE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, buttonTypeOk);
    }

    @Override
    protected Node createInterior() {
        GridPane grid = new GridPane();
        grid.add(new Label(Messages.CUSTOMER_NAME.createMsg() + ": "), 1, 1);
        grid.add(name = new TextField(), 2, 1);
        return grid;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        try {
            EshopFacade.getService().createCustomer(name.getText());
            CustomerObservable.instance.markChangedAndNotifyObservers();
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }
}
