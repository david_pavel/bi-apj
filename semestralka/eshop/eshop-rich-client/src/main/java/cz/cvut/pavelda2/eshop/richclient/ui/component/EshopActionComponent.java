package cz.cvut.pavelda2.eshop.richclient.ui.component;

/**
 * Created by David Pavel on 3. 11. 2015.
 */
public interface EshopActionComponent {
    public void setDisable(boolean disable);
}
