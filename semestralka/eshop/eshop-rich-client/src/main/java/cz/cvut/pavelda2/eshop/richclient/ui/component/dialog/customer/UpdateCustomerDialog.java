package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.CustomerId;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.CustomerObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.OrderObservable;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class UpdateCustomerDialog extends AbstractCreateDialog {

    private final CustomerId customerId;
    private TextField id;
    private TextField name;

    public UpdateCustomerDialog(CustomerId id) throws EshopException {
        customerId = id;

        setTitle(Messages.CUSTOMER_UPDATE_DIALOG_TITLE.createMsg());
        this.initModality(Modality.APPLICATION_MODAL);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_SAVE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL,buttonTypeOk);
    }

    @Override
    protected Node createInterior() throws EshopException {

        Customer customer = EshopFacade.getService().findCustomerById(customerId.getId());

        id = new TextField(customer.getId().toString());
        id.setEditable(false);
        name = new TextField(customer.getName());

        GridPane grid = new GridPane();
        grid.add(new Label(Messages.ID.createMsg() + ": "),1,1);
        grid.add(id,2,1);
        grid.add(new Label(Messages.CUSTOMER_NAME.createMsg() + ": "),1,2);
        grid.add(name,2,2);
        return grid;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        try {
            EshopFacade.getService().updateCustomer(
                    customerId.getId(),
                    name.getText());

            CustomerObservable.instance.markChangedAndNotifyObservers();
            OrderObservable.instance.markChangedAndNotifyObservers(); // Update also Customers (e.g. Customer names for orders}
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }
}
