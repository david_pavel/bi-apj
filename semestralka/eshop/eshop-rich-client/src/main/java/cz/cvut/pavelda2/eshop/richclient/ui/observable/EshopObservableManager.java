package cz.cvut.pavelda2.eshop.richclient.ui.observable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David Pavel on 10. 11. 2015.
 */
public class EshopObservableManager {

    public static EshopObservableManager instance = new EshopObservableManager();
    List<AbstractEshopObservable> observables = new ArrayList<>();

    public void notifyAllObservebles() {
        for (AbstractEshopObservable observable : observables) {
            observable.markChangedAndNotifyObservers();
        }
    }

    public void registerObservable(AbstractEshopObservable abstractEshopObservable) {
        this.observables.add(abstractEshopObservable);
    }
}
