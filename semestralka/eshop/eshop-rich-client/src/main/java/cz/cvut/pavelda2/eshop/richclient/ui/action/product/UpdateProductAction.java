package cz.cvut.pavelda2.eshop.richclient.ui.action.product;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.product.UpdateProductDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.ProductsPanel;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class UpdateProductAction extends AbstractAction {

    public static final AbstractAction instance = new UpdateProductAction();

    public UpdateProductAction() {
        super(Messages.PRODUCT_UPDATE.createMsg());
    }

    @Override
    public void execute() {
        Product product = ProductsPanel.getInstance().getTable().getSelectionModel().getSelectedItems().get(0);
        try {
            AbstractCreateDialog dialog = new UpdateProductDialog(product.getId());
            dialog.execute();
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }

    @Override
    public boolean isDisabled() {
        return !EshopFacade.getService().facadeAvailable() || ProductsPanel.getInstance().getTable().getSelectionModel().getSelectedItems().size() != 1;
    }
}
