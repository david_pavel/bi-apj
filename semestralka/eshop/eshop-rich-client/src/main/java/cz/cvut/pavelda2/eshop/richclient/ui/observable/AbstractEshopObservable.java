package cz.cvut.pavelda2.eshop.richclient.ui.observable;

import java.util.Observable;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public abstract class AbstractEshopObservable extends Observable {
    public AbstractEshopObservable() {
        EshopObservableManager.instance.registerObservable(this);
    }

    public void markChanged() {
        setChanged();
    }

    public void markChangedAndNotifyObservers() {
        markChanged();
        notifyObservers();
    }
}
