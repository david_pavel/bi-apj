package cz.cvut.pavelda2.eshop.richclient.ui.action.order;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.order.CreateOrderDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.product.CreateProductDialog;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateOrderAction extends AbstractAction {

    public static final AbstractAction instance = new CreateOrderAction();

    public CreateOrderAction() {
        super(Messages.ORDER_ADD.createMsg());
    }

    @Override
    public void execute() {
        AbstractCreateDialog createDialog = new CreateOrderDialog();
        createDialog.execute();
    }

    @Override
    public boolean isDisabled() {
        return!EshopFacade.getService().facadeAvailable();
    }
}
