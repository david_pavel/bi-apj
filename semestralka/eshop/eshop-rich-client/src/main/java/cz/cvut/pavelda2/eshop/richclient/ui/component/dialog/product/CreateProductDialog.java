package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.product;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.ProductObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateProductDialog extends AbstractCreateDialog {

    private TextField price;
    private TextField name;

    public CreateProductDialog() {
        setTitle(Messages.PRODUCT_CREATE_DIALOG_TITLE.createMsg());
        this.initModality(Modality.APPLICATION_MODAL);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_CREATE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL,buttonTypeOk);
    }

    @Override
    protected Node createInterior() {

        name = new TextField();
        price = new TextField();

        GridPane grid = new GridPane();
        grid.add(new Label(Messages.PRODUCT_NAME.createMsg() + ": "),1,1);
        grid.add(name,2,1);
        grid.add(new Label(Messages.PRODUCT_PRICE.createMsg() + ": "), 1, 2);
        grid.add(price,2,2);
        return grid;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        try {
            EshopFacade.getService().createProduct(
                    name.getText(),
                    Double.parseDouble(price.getText()));

            ProductObservable.instance.markChangedAndNotifyObservers();
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }
}
