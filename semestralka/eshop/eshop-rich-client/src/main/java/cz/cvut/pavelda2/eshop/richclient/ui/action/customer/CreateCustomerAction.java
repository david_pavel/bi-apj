package cz.cvut.pavelda2.eshop.richclient.ui.action.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.customer.CreateCustomerDialog;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateCustomerAction extends AbstractAction {

    public static final AbstractAction instance = new CreateCustomerAction();

    public CreateCustomerAction() {
        super(Messages.CUSTOMER_ADD.createMsg());
    }

    @Override
    public void execute() {
        new CreateCustomerDialog().execute();
    }

    @Override
    public boolean isDisabled() {
        return !EshopFacade.getService().facadeAvailable();
    }
}
