package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.product;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.OrderObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.ProductObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class UpdateProductDialog extends AbstractCreateDialog {

    private final ProductId productId;
    private TextField id;
    private TextField price;
    private TextField name;

    public UpdateProductDialog(ProductId id) throws EshopException {
        productId = id;

        setTitle(Messages.PRODUCT_UPDATE_DIALOG_TITLE.createMsg());
        this.initModality(Modality.APPLICATION_MODAL);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_SAVE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL,buttonTypeOk);
    }

    @Override
    protected Node createInterior() throws EshopException {

        Product product = EshopFacade.getService().findProductById(productId.getId());

        id = new TextField(product.getId().toString());
        id.setEditable(false);
        name = new TextField(product.getName());
        price = new TextField(product.getPrice().toString());

        GridPane grid = new GridPane();
        grid.add(new Label(Messages.ID.createMsg() + ": "),1,1);
        grid.add(id,2,1);
        grid.add(new Label(Messages.PRODUCT_NAME.createMsg() + ": "),1,2);
        grid.add(name,2,2);
        grid.add(new Label(Messages.PRODUCT_PRICE.createMsg() + ": "), 1, 3);
        grid.add(price,2,3);
        return grid;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        try {
            EshopFacade.getService().updateProduct(
                    productId.getId(),
                    name.getText(),
                    Double.parseDouble(price.getText()));

            ProductObservable.instance.markChangedAndNotifyObservers();
            OrderObservable.instance.markChangedAndNotifyObservers(); // Update also Products (e.g. Product names for orders}
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }
}
