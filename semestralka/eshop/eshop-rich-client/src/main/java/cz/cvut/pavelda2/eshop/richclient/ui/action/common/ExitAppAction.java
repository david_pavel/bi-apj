package cz.cvut.pavelda2.eshop.richclient.ui.action.common;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.window.MainWindow;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 3. 11. 2015.
 */
public class ExitAppAction extends AbstractAction {

    public static AbstractAction instance = new ExitAppAction();

    public ExitAppAction() {
        super(Messages.EXIT.createMsg(), KeyCombination.keyCombination("CTRL+Q"));
    }

    @Override
    public void execute() {
        MainWindow.instance.stop();
    }

    @Override
    public boolean isDisabled() {
        return false;
    }
}
