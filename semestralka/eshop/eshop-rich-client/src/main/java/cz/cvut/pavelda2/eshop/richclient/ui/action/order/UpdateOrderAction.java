package cz.cvut.pavelda2.eshop.richclient.ui.action.order;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.OrderPanel;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.ProductsPanel;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class UpdateOrderAction extends AbstractAction {

    public static final AbstractAction instance = new UpdateOrderAction();

    public UpdateOrderAction() {
        super(Messages.ORDER_UPDATE.createMsg());
    }

    @Override
    public void execute() {
        throw new RuntimeException("Update order not implemeted yet");
//        AbstractCreateDialog createProductDialog = new CreateProductDialog();
//        createProductDialog.execute();
    }

    @Override
    public boolean isDisabled() {
        return OrderPanel.getInstance().getTable().getSelectionModel().getSelectedItems().size() != 1
                || !EshopFacade.getService().facadeAvailable();
    }
}
