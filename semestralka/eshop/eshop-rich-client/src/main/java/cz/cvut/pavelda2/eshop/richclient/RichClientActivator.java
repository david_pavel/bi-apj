package cz.cvut.pavelda2.eshop.richclient;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.richclient.ui.action.ActionState;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.window.MainWindow;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class RichClientActivator implements BundleActivator {
    private Logger log = Logger.getLogger(RichClientActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Rich client module started <<<");
        /*// ------------------------------------------------
        log.info("Populating database ...");
        DatabasePopulator databasePopulator = new TestDatabasePopulator();
        databasePopulator.populateDatabase();
        log.info("Database populated");
        // ------------------------------------------------*/
        new JFXPanel();
        Platform.runLater(() -> {
            MainWindow.instance.setBundleContext(bundleContext);
            ActionState.instance.fireUpdate();
            Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
                EshopAlert.error(e); // set default exception handler
            });
        });
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Rich client module stopped <<<");
    }
}
