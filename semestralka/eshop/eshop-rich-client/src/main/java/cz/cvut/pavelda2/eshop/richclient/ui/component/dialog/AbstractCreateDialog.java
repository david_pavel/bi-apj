package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog;

import java.util.Optional;

import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

/**
 * Created by David Pavel on 10. 11. 2015.
 */
public abstract class AbstractCreateDialog extends Dialog<ButtonType> {
    protected abstract Node createInterior() throws EshopException;

    public void execute() {
        Optional<ButtonType> result = this.showAndWait();
        ButtonType buttonType = result.get();
        if (buttonType.getButtonData().isCancelButton()) {
            return;
        }

        onSubmit(buttonType);
    }



    protected abstract void onSubmit(ButtonType buttonType);
}
