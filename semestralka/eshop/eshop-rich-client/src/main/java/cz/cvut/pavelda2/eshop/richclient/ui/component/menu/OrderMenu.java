package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.CreateOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.DeleteOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.UpdateOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.CreateProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.DeleteProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.UpdateProductAction;
import javafx.scene.control.Menu;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class OrderMenu extends Menu {


    public OrderMenu() {
        super(Messages.MENU_ORDERS.createMsg());
        getItems().addAll(
                CreateOrderAction.instance.createMenuItem(),
                UpdateOrderAction.instance.createMenuItem(),
                DeleteOrderAction.instance.createMenuItem()
        );
    }
}
