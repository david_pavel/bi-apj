package cz.cvut.pavelda2.eshop.richclient.ui.component.panel;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.CustomerId;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.CustomerObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.CreateCustomerAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.DeleteCustomerAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.customer.UpdateCustomerAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.table.EshopTableView;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Created by David Pavel on 3. 11. 2015.
 */
public class CustomerPanel extends TitledPane implements Observer {
    private static CustomerPanel instance;
    public static CustomerPanel getInstance() {
        if (instance == null) {
            instance = new CustomerPanel();
        }
        return  instance;
    }

    // Dynamic reloding structure
    protected ObservableList<Customer> customers = FXCollections.observableArrayList();

    private final TableView<Customer> table;

    private CustomerPanel() {
        super(Messages.CUSTOMERS_LIST.createMsg(), null);
        setContent(table = createCustomerTable());
        refresh();
        CustomerObservable.instance.addObserver(this);
    }

    public void refresh() {
        if (EshopFacade.getService().facadeAvailable()) {
            try {
                Collection<Customer> allCustomers = EshopFacade.getService().getAllCustomers();
                customers.clear();
                customers.addAll(allCustomers);
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
    }

    protected TableView<Customer> createCustomerTable() {
        TableView<Customer> table = new EshopTableView<Customer>() {
            @Override
            public void onUpdateRowEvent() {
                UpdateCustomerAction.instance.execute();
            }
            @Override
            public void onDeleteRowEvent() {
                DeleteCustomerAction.instance.execute();
            }
            @Override
            public void onInsertRowEvent() {
                CreateCustomerAction.instance.execute();
            }
        };
        TableColumn<Customer, CustomerId> idCol = new TableColumn(Messages.ID.createMsg());
        idCol.setCellValueFactory(new PropertyValueFactory<Customer, CustomerId>("id"));
        TableColumn<Customer, CustomerId> nameCol = new TableColumn(Messages.CUSTOMER_NAME.createMsg());
        nameCol.setCellValueFactory(new PropertyValueFactory<Customer, CustomerId>("name"));
        table.getColumns().addAll(idCol, nameCol);
        table.setItems(customers);
        table.setContextMenu(new ContextMenu(
                UpdateCustomerAction.instance.createMenuItem(),
                DeleteCustomerAction.instance.createMenuItem()
        ));
        return table;
    }

    @Override
    public void update(Observable o, Object arg) {
        refresh();
    }

    public TableView<Customer> getTable() {
        return table;
    }
}
