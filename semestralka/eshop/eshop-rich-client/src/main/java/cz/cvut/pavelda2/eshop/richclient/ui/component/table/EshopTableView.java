package cz.cvut.pavelda2.eshop.richclient.ui.component.table;

import cz.cvut.pavelda2.eshop.richclient.ui.action.ActionState;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public abstract class EshopTableView<T> extends TableView<T> {
    public EshopTableView() {
        this(FXCollections.<T>observableArrayList());
    }

    public EshopTableView(ObservableList<T> items) {
        super(items);
        getSelectionModel().getSelectedCells().addListener((Observable observable) -> {
            ActionState.instance.fireUpdate();
        });
        getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case F8:
                case DELETE:
                    onDeleteRowEvent();
                    break;
                case INSERT:
                    onInsertRowEvent();
                    break;
                case F2:
                    onUpdateRowEvent();
                    break;
            }
        });
        setOnMouseClicked(event -> {
            if (event.getClickCount() > 1) {
                onUpdateRowEvent();
            }
        });

    }

    public abstract void onUpdateRowEvent();

    public abstract void onDeleteRowEvent();

    public abstract void onInsertRowEvent();
}
