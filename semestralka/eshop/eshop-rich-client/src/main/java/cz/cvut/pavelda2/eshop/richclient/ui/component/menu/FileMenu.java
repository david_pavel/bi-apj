package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.common.ExitAppAction;
import javafx.scene.control.Menu;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class FileMenu extends Menu {

    public FileMenu() {
        super(Messages.MENU_FILE.createMsg());
        getItems().addAll(
                ExitAppAction.instance.createMenuItem()
        );
    }
}
