package cz.cvut.pavelda2.eshop.richclient.ui.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by David Pavel on 1. 12. 2015.
 */
public class ActionState implements Observer {
    public final static ActionState instance = new ActionState();

    private final Collection<AbstractAction> actions = new ArrayList<>();

    @Override
    public void update(Observable o, Object arg) {
       fireUpdate();
    }

    public void fireUpdate() {
        for (AbstractAction eshopAction : actions) {
            eshopAction.checkDisable();
        }
    }

    public void registerAction(AbstractAction action) {
        actions.add(action);
    }

}
