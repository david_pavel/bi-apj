package cz.cvut.pavelda2.eshop.richclient.ui.action.customer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.CustomerObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.CustomerPanel;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.ObservableList;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class DeleteCustomerAction extends AbstractAction {

    public static final AbstractAction instance = new DeleteCustomerAction();

    public DeleteCustomerAction() {
        super(Messages.CUSTOMER_DELETE.createMsg());
    }

    @Override
    public boolean isDisabled() {
        return CustomerPanel.getInstance().getTable().getSelectionModel().getSelectedItems().isEmpty()
                || !EshopFacade.getService().facadeAvailable();
    }

    @Override
    public void execute() {
        ObservableList<Customer> selectedItems = CustomerPanel.getInstance().getTable().getSelectionModel().getSelectedItems();
        for (Customer selectedItem : selectedItems) {
            try {
                EshopFacade.getService().removeCustomer(selectedItem.getId().getId());
                CustomerObservable.instance.markChanged();
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
        CustomerObservable.instance.notifyObservers();
    }
}
