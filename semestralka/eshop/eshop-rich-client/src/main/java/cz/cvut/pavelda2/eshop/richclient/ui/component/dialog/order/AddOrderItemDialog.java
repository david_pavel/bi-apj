package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.order;

import java.util.Collection;
import java.util.Currency;
import java.util.Locale;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.OrderItem;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class AddOrderItemDialog extends AbstractCreateDialog {
    protected ObservableList<Product> products = FXCollections.observableArrayList();

    private ComboBox<Product> productCB;
    private TextField countTxt;
    private Label priceLbl;
    private OrderItem orderItem = new OrderItem();

    public AddOrderItemDialog() {
        // Dialog config
        setTitle(Messages.ADD_ORDER_ITEM_DIALOG_TITLE.createMsg());
        setResizable(true);

        // Gui config
        this.getDialogPane().setContent(createInterior());
        initData();

        // Buttons config
        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_CREATE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, buttonTypeOk);
    }

    private ComboBox<Product> createCustomersCB() {
        productCB = new ComboBox<>(products);
        productCB.setPromptText("<Vyberte produkt>");
        productCB.setCellFactory((p) -> createProductListCell());
        productCB.setButtonCell(createProductListCell());
        return productCB;
    }

    private ListCell<Product> createProductListCell() {
        return new ListCell<Product>() {
            @Override
            protected void updateItem(Product product, boolean empty) {
                super.updateItem(product, empty);
                if (product != null) {
                    setText(product.getName() + " - " + product.getPrice());
                } else {
                    setText(null);
                }
            }
        };
    }

    private void initData() {
        try {
            Collection<Product> allProducts = EshopFacade.getService().getAllProducts();
            products.clear();
            products.addAll(allProducts);
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }

    @Override
    protected Node createInterior() {
        GridPane grid = new GridPane();
        grid.add(new Label(Messages.PRODUCT.createMsg() + ": "), 1, 1);
        grid.add(productCB = createCustomersCB(), 2, 1);
        productCB.valueProperty().addListener((observable, oldValue, newValue) -> {
            orderItem.setProduct(newValue);
            updatePrice();
        });

        grid.add(new Label("Počet kusů: "), 1, 2);
        grid.add(countTxt = new TextField(), 2, 2);
        countTxt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.trim().isEmpty()) {
                orderItem.setCount(null);
            } else {
                orderItem.setCount(Integer.parseInt(newValue));
            }
            updatePrice();
        });

        grid.add(new Label("Cena: "), 1, 3);
        grid.add(priceLbl = new Label("- Kč"), 2, 3);

        return grid;
    }

    private void updatePrice() {
        if (orderItem.getCount() == null || orderItem.getProduct() == null) {
            // Can not compute
            priceLbl.setText("- Kč");
            return;
        }

        // Compute price
        Double result = orderItem.getProduct().getPrice() * orderItem.getCount();
        priceLbl.setText(String.format("%10.2f Kč", result));
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        /* nothing to do*/
    }

    public OrderItem getOrderItem() {
        return this.orderItem;
    }
}
