package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.richclient.ui.component.EshopActionComponent;
import javafx.scene.control.MenuItem;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class EshopMenuItem extends MenuItem implements EshopActionComponent {
    public EshopMenuItem(String name) {
        super(name);
    }
}