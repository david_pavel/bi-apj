package cz.cvut.pavelda2.eshop.richclient.ui.component.panel;

import java.text.NumberFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Order;
import cz.cvut.pavelda2.eshop.model.entity.OrderId;
import cz.cvut.pavelda2.eshop.model.entity.OrderItem;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.OrderObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.CreateOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.DeleteOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.UpdateOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.table.EshopTableView;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Created by David Pavel on 3. 11. 2015.
 */
public class OrderPanel extends TitledPane implements Observer {
    private static OrderPanel instance;
    public static OrderPanel getInstance() {
        if (instance == null) {
            instance = new OrderPanel();
        }
        return  instance;
    }

    // Dynamic reloading structure
    protected ObservableList<Order> orders = FXCollections.observableArrayList();

    private final TableView<Order> table;

    private OrderPanel() {
        super(Messages.ORDERS_LIST.createMsg(), null);
        setContent(table = createOrderTable());
        refresh();
        OrderObservable.instance.addObserver(this);
    }

    public void refresh() {
        if (EshopFacade.getService().facadeAvailable()) {
            try {
                Collection<Order> allOrders = EshopFacade.getService().getAllOrders();
                orders.clear();
                orders.addAll(allOrders);
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
    }


    protected TableView<Order> createOrderTable() {
        TableView<Order> table = new EshopTableView<Order>() {
            @Override
            public void onUpdateRowEvent() {
                UpdateOrderAction.instance.execute();
            }
            @Override
            public void onDeleteRowEvent() {
                DeleteOrderAction.instance.execute();
            }
            @Override
            public void onInsertRowEvent() {
                CreateOrderAction.instance.execute();
            }
        };
        TableColumn<Order, OrderId> idCol = new TableColumn(Messages.ID.createMsg());
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn<Order, String> nameCol = new TableColumn(Messages.CUSTOMER.createMsg());
        nameCol.setCellValueFactory(order -> new SimpleStringProperty(order.getValue().getCustomer().getName()));
        TableColumn<Order, String> itemsCol = new TableColumn(Messages.PRODUCTS_COUNT.createMsg());
        itemsCol.setCellValueFactory(order -> new SimpleObjectProperty(
                new Integer(order.getValue().getItems()
                        .stream()
                        .mapToInt(OrderItem::getCount)
                        .sum())));
        TableColumn<Order, String> priceCol = new TableColumn(Messages.TOTAL_PRICE.createMsg());
        priceCol.setCellValueFactory(order -> {
            SimpleStringProperty property = new SimpleStringProperty();
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("cs", "cz"));
            property.setValue(numberFormat.format(order.getValue().getItems()
                    .stream()
                    .mapToDouble((OrderItem oi) -> oi.getCount() * oi.getProduct().getPrice())
                    .sum()));
            return property;
        });
        table.getColumns().addAll(idCol, nameCol, itemsCol, priceCol);
        table.setItems(orders);
        table.setContextMenu(new ContextMenu(
                DeleteOrderAction.instance.createMenuItem(),
                UpdateOrderAction.instance.createMenuItem()
        ));
        return table;
    }

    @Override
    public void update(Observable o, Object arg) {
        refresh();
    }

    public TableView<Order> getTable() {
        return table;
    }
}
