package cz.cvut.pavelda2.eshop.richclient.ui.component.alert;

import java.text.MessageFormat;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import javafx.scene.control.Alert;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class EshopAlert  {
    public static void error(String pattern, Throwable throwable, Object... arguments) {
        if (throwable != null) {
            throwable.printStackTrace();
        }


        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setContentText(MessageCreator.createFor(pattern, throwable, arguments));
        a.showAndWait();
    }

    public static void error(String pattern, Object... arguments) {
        error(pattern, null, arguments);
    }

    public static void error(Throwable throwable) {
        error(null, throwable);
    }

    public static void success(String pattern, Object ... arguments) {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setContentText(MessageCreator.createFor(pattern, null, arguments));
        a.showAndWait();
    }

    static class MessageCreator {
        public static String createFor(String pattern, Throwable throwable, Object[] arguments) {
            StringBuilder sb = new StringBuilder();

            if (pattern != null) {
                sb.append(MessageFormat.format(pattern, arguments));
            }

            if (throwable != null) {
                sb.append(throwable.toString());
            }

            if (sb.length() == 0) {
                sb.append(Messages.UNEXPECTED_PROBLEM_ENCOUNTRED.createMsg());
            }

            return sb.toString();
        }
    }
}
