package cz.cvut.pavelda2.eshop.richclient.ui.action.order;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.Order;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.OrderObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.panel.OrderPanel;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.collections.ObservableList;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class DeleteOrderAction extends AbstractAction {

    public static final AbstractAction instance = new DeleteOrderAction();

    public DeleteOrderAction() {
        super(Messages.ORDER_DELETE.createMsg());
    }

    @Override
    public boolean isDisabled() {
        return OrderPanel.getInstance().getTable().getSelectionModel().getSelectedItems().isEmpty()
                || !EshopFacade.getService().facadeAvailable();
    }

    @Override
    public void execute() {
        ObservableList<Order> selectedItems = OrderPanel.getInstance().getTable().getSelectionModel().getSelectedItems();
        for (Order selectedItem : selectedItems) {
            try {
                EshopFacade.getService().removeOrder(selectedItem.getId().getId());
                OrderObservable.instance.markChanged();
            } catch (EshopException e) {
                EshopAlert.error(e);
            }
        }
        OrderObservable.instance.notifyObservers();
    }
}
