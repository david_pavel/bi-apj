package cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.order;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.model.entity.*;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.DeleteOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.order.UpdateOrderAction;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.OrderObservable;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateOrderDialog extends AbstractCreateDialog {
    protected ObservableList<Customer> customers = FXCollections.observableArrayList();
    private ObservableList<OrderItem> items = FXCollections.observableArrayList();

    private ComboBox<Customer> customerCB = new ComboBox<>(customers);
    private Label totalPrice;
    private Button addItemButton;

    public CreateOrderDialog() {
        initData();
        initCustomersCB();


        setTitle(Messages.ORDER_CREATE_DIALOG_TITLE.createMsg());
        setResizable(true);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_CREATE.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, buttonTypeOk);
    }

    private void initCustomersCB() {
        customerCB.setPromptText("<Vyberte zákazníka>");
        customerCB.setCellFactory((p) -> createCustomerListCell());
        customerCB.setButtonCell(createCustomerListCell());
    }

    private ListCell<Customer> createCustomerListCell() {
        return new ListCell<Customer>() {
            @Override
            protected void updateItem(Customer customer, boolean empty) {
                super.updateItem(customer, empty);
                if (customer != null) {
                    setText(customer.getName());
                } else {
                    setText(null);
                }
            }
        };
    }

    private void initData() {
        try {
            Collection<Customer> allCustomers = EshopFacade.getService().getAllCustomers();
            customers.clear();
            customers.addAll(allCustomers);
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }

    @Override
    protected Node createInterior() {
        GridPane grid = new GridPane();
        grid.add(new Label(Messages.CUSTOMER.createMsg() + ": "), 1, 1);
        grid.add(customerCB, 2, 1);

        grid.add(new Label("Položky objednávky: "), 1, 2);
        grid.add(createItemsTable(),1,3,2,1);
        grid.add(new Label("Cena celkem: "), 1, 4);
        grid.add(totalPrice = new Label("- Kč"), 2, 4);

        grid.add(addItemButton = new Button("Přidat položku"),2,5);
        addItemButton.onActionProperty().set(event -> {
            AddOrderItemDialog dialog = new AddOrderItemDialog();
            Optional<ButtonType> buttonType = dialog.showAndWait();
            if (!buttonType.get().getButtonData().isCancelButton()) {
                items.add(dialog.getOrderItem());
                recomputeTotalPrice();
            }
        });

        return grid;
    }

    private void recomputeTotalPrice() {
        totalPrice.setText(Double.valueOf(items.stream().mapToDouble(i -> i.getPrice()).sum()).toString() + " Kč");
    }

    private TableView<OrderItem> createItemsTable() {
        TableView<OrderItem> table = new TableView<>(items);

        TableColumn<OrderItem, String> nameCol = new TableColumn("Product name");
        nameCol.setCellValueFactory(orderItem -> new SimpleObjectProperty(
                orderItem.getValue().getProduct().getName()));

        TableColumn<OrderItem, Integer> countCol = new TableColumn("Count");
        countCol.setCellValueFactory(orderItem -> new SimpleObjectProperty(
                orderItem.getValue().getCount()));

        TableColumn<OrderItem, Integer> priceCol = new TableColumn("Price");
        priceCol.setCellValueFactory(orderItem -> new SimpleObjectProperty(
                orderItem.getValue().getPrice()));

        table.getColumns().addAll( nameCol, countCol, priceCol);
        return table;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        try {
            EshopFacade.getService().createOrder(new ArrayList<>(items), customerCB.getSelectionModel().getSelectedItem());
            OrderObservable.instance.markChangedAndNotifyObservers();
        } catch (EshopException e) {
            EshopAlert.error(e);
        }
    }
}
