package cz.cvut.pavelda2.eshop.richclient.ui.component.menu;

import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.CreateProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.DeleteProductAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.product.UpdateProductAction;
import javafx.scene.control.Menu;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class ProductMenu extends Menu {


    public ProductMenu() {
        super(Messages.MENU_PRODUCTS.createMsg());
        getItems().addAll(
                CreateProductAction.instance.createMenuItem(),
                UpdateProductAction.instance.createMenuItem(),
                DeleteProductAction.instance.createMenuItem()
        );
    }
}
