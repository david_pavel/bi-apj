package cz.cvut.pavelda2.eshop.connection;

import java.io.*;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.utils.Marshaller;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 9. 1. 2016.
 */
public class NioConnection extends AbstractConnection {


    static final Logger LOG = Logger.getLogger(NioConnection.class.getName());
    private static final NioConnection instance = new NioConnection();
    private DataInputStream dis;
    private DataOutputStream dos;
    private Socket socket;

    /**
     * @return the instance
     */
    public static NioConnection getService() {
        return instance;
    }

    private static Object readObject(DataInputStream dis) throws IOException {
        int len = dis.readShort();
        byte[] result = new byte[len];
        dis.read(result);
        return Marshaller.bytes2Object(result);
    }

    @Override
    public void connect(String host, int port) throws EshopException {
        if (isConnected()) {
            return;
        }
        try {

            socket = new Socket(host, port);
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            socket.setSoTimeout(2000);
        } catch (IOException e) {
            throw new EshopException(e);
        }

    }

    @Override
    public <T> T send(AbstractCommand comm) throws EshopException {

        // logger.info(socket.toString());
        if (socket == null) {
            throw new EshopException("Not connected");
        }
        LOG.info(MessageFormat.format("Sending comand: {0}", comm.toString()));
        try {
            byte[] commBytes = Marshaller.obj2Bytes(comm);
            dos.writeShort(commBytes.length);
            dos.write(commBytes);
            dos.flush();

            if (comm instanceof  LogoutCommand) {
                return null; // nothing to return
            }

            Object result = readObject(dis);
            LOG.info(MessageFormat.format("Response: {0}", result));
            if (result instanceof EshopException) {
                throw (EshopException) result;
            }
            return (T) result;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new EshopException(ex);
        }
    }

    @Override
    public boolean isConnected() {
        return socket != null;
    }

    @Override
    public void disconnect() {
        if (!isConnected()) {
            return;
        }
        try {
            send(new LogoutCommand());
        } catch (EshopException ex) {
            Logger.getLogger(NioConnection.class
                    .getName()).info(ex.toString());
        }
        try (Socket socket = this.socket;
             InputStream is = this.dis;
             OutputStream os = this.dos) {

        } catch (IOException ex) {
        }
        socket = null;
    }
}
