package cz.cvut.pavelda2.eshop.connection;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class NioConnectionActivator implements BundleActivator {
    private Logger log = Logger.getLogger(NioConnectionActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> NIO Connection module started <<<");
        bundleContext.registerService(AbstractConnection.class.getName(), new NioConnection(), null);
        log.info("NIO Connection registred.");


    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> NIO Connection module stopped <<<");
    }
}
