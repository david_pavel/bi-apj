package cz.cvut.pavelda2.eshop.proxy;

import java.util.List;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.Order;
import cz.cvut.pavelda2.eshop.model.entity.OrderItem;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.protocol.command.customer.*;
import cz.cvut.pavelda2.eshop.protocol.command.order.GetAllOrdersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.product.*;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class EshopFacadeProxy extends EshopFacade {

    // ----- Facade ----------------------------------------------------------------------

    @Override
    public boolean facadeAvailable() {
        return AbstractConnection.getService().isConnected();
    }

    // ----- Products ----------------------------------------------------------------------

    @Override
    public Product createProduct(String name, double price) throws EshopException {
        AbstractConnection.getService().send(new CreateProductCommand(name, price));
        return null;
    }

    @Override
    public Product updateProduct(Integer id, String name, double price) throws EshopException {
        AbstractConnection.getService().send(new UpdateProductCommand(id, name, price));
        return null;
    }

    @Override
    public void removeProduct(Integer id) throws EshopException {
        AbstractConnection.getService().send(new RemoveProductCommand(id));
    }

    @Override
    public Product findProductById(Integer id) throws EshopException {
        return AbstractConnection.getService().send(new FindProductByIdCommand(id));
    }

    @Override
    public List<Product> getAllProducts() throws EshopException {
        return AbstractConnection.getService().send(new GetAllProductsCommand());
    }

    // ----- Customers ----------------------------------------------------------------------

    @Override
    public Customer createCustomer(String name) throws EshopException {
        return AbstractConnection.getService().send(new CreateCustomerCommand(name));

    }

    @Override
    public Customer updateCustomer(Integer id, String name) throws EshopException {
        return AbstractConnection.getService().send(new UpdateCustomerCommand(id, name));

    }

    @Override
    public void removeCustomer(Integer id) throws EshopException {
        AbstractConnection.getService().send(new RemoveCustomerCommand(id));

    }

    @Override
    public Customer findCustomerById(Integer id) throws EshopException {
        return AbstractConnection.getService().send(new FindCustomerByIdCommand(id));
    }

    @Override
    public List<Customer> getAllCustomers() throws EshopException {
        return AbstractConnection.getService().send(new GetAllCustomersCommand());

    }

    // ----- Orders ----------------------------------------------------------------------
    @Override
    public Order createOrder(List<OrderItem> orderItems, Customer customer) throws EshopException {
        return AbstractConnection.getService().send(new CreateOrderCommand(orderItems, customer));
    }

    @Override
    public void removeOrder(Integer id) throws EshopException {
        AbstractConnection.getService().send(new RemoveOrderCommand(id));

    }

    @Override
    public List<Order> getAllOrders() throws EshopException {
        return AbstractConnection.getService().send(new GetAllOrdersCommand());
    }
}
