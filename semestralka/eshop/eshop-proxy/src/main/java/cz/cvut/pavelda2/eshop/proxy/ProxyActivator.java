package cz.cvut.pavelda2.eshop.proxy;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class ProxyActivator implements BundleActivator {
    private Logger log = Logger.getLogger(ProxyActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Proxy module started <<<");
        bundleContext.registerService(EshopFacade.class.getName(), new EshopFacadeProxy(), null);
        log.info("EshopFacade PROXY registred.");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Proxy module stopped <<<");
    }
}
