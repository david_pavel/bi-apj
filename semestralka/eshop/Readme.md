# BI-APJ: E-Shop

## Doména aplikace

#### Entity
* Produkt
* Zákazník
* Objednávka
* Položka objednávky

## Úpravy navíc

1. Napojení na PostgreSQL (Embedded dependency pro felix)
2. Globlní exception handler
```java
Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
    EshopAlert.error(e.getLocalizedMessage()); // set default exception handler
});
```
3. Context menu tabulek
4. Shortcuts: Quit application (`CTRL+Q`), ...
5. EshopTableView: events `DELETE`, `INS` keys pressed
6. Toolbar
7. Pokročilý formulář (Přidání objednávky) + výpočet ceny v reálném čase
7. Zahrnutí `src/main/resources` pomocí `<Include-Resource>{maven-resources}</Include-Resource>`
8. Nio Server
9. **Vyřešení workaround s `ClassNotFoundException` u serveru pri nacitani Commands**

Dříve:
```
    private Class[] _classes = {GetAllProductsCommand.class, GetAllCustomersCommand.class,
          GetAllOrdersCommand.class, LogoutCommand.class};
```
Nyní stačí přidat do modulu, kde budeme tyto třídy používat,
 přidat do konfigurace `maven-bundle-plugin` v `pom.xml` element `<DynamicImport-Package>...</DynamicImport-Package>`.
```
<plugin>
    <groupId>org.apache.felix</groupId>
    <artifactId>maven-bundle-plugin</artifactId>
    <extensions>true</extensions>
    <configuration>
        <instructions>
            <Bundle-Activator>cz.cvut.pavelda2.eshop.utils.UtilsActivator</Bundle-Activator>
            <Export-Package>
                cz.cvut.pavelda2.eshop.utils,
                cz.cvut.pavelda2.eshop.utils.exception,
                cz.cvut.pavelda2.eshop.utils.json
            </Export-Package>
            <DynamicImport-Package>
                cz.cvut.pavelda2.eshop.model.entity,
                cz.cvut.pavelda2.eshop.model.entity.*,
                cz.cvut.pavelda2.eshop.protocol.command,
                cz.cvut.pavelda2.eshop.protocol.command.product,
                cz.cvut.pavelda2.eshop.protocol.command.order,
                cz.cvut.pavelda2.eshop.protocol.command.customer,
                cz.cvut.pavelda2.eshop.protocol.command.*
            </DynamicImport-Package>
        </instructions>
    </configuration>
</plugin>
```
