package cz.cvut.pavelda2.eshop.server.nio;

import java.util.concurrent.Executors;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.server.nio.tasks.NioServer;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class NioServerActivator implements BundleActivator {
    private static final int DEFAULT_PORT = 1234;
    private Logger log = Logger.getLogger(NioServerActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> NIO Server module started <<<");

        Executors.newCachedThreadPool().execute(new NioServer(DEFAULT_PORT));
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> NIO Server module stopped <<<");
    }
}
