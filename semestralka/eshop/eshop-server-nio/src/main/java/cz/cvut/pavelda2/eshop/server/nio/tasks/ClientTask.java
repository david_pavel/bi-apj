package cz.cvut.pavelda2.eshop.server.nio.tasks;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.protocol.command.customer.GetAllCustomersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.order.GetAllOrdersCommand;
import cz.cvut.pavelda2.eshop.protocol.command.product.GetAllProductsCommand;
import cz.cvut.pavelda2.eshop.utils.Marshaller;
import cz.cvut.pavelda2.eshop.utils.NumberSequence;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public class ClientTask implements Runnable {
    private static final Logger LOG = Logger.getLogger(ClientTask.class.getName());
    private static final int SHORT_LENGTH = Short.SIZE / Byte.SIZE;

    private final byte[] commBytes;
    private final SocketChannel socketChannel;

    public ClientTask(byte[] commBytes, SocketChannel socketChannel) {
        this.commBytes = commBytes;
        this.socketChannel = socketChannel;
    }

    @Override
    public void run() {
        try {
            Object result;
            try {
                AbstractCommand command = (AbstractCommand) Marshaller.bytes2Object(commBytes);
                LOG.info(MessageFormat.format("New command: {0}", command.toString()));
                if (command instanceof LogoutCommand) {
                    socketChannel.close();
                    return;
                }
                result = command.execute(EshopFacade.getService());
                LOG.info(MessageFormat.format("Command response: {0}", result));
            } catch (EshopException ex) {
                Logger.getLogger(ClientTask.class.getName()).log(Level.SEVERE, null, ex);
                result = ex;
            }
            byte[] resultBytes = Marshaller.obj2Bytes(result);
            ByteBuffer writeBuff = ByteBuffer.allocate(resultBytes.length + SHORT_LENGTH);
            writeBuff.putShort((short) resultBytes.length);
            writeBuff.put(resultBytes);
            writeBuff.flip();
            socketChannel.write(writeBuff);
        } catch (IOException ex) {
            Logger.getLogger(ClientTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
