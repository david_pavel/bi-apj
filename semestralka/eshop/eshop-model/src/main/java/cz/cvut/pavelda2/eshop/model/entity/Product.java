package cz.cvut.pavelda2.eshop.model.entity;

import java.io.Serializable;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class Product implements Serializable {
    static final long serialVersionUID = 1L;

    private ProductId id;
    private Double price;
    private String name;

    private Product(){};

    public Product(String name, Double price) {
        this(null, name, price);
    }

    public Product(ProductId id, String name, Double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public ProductId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public void setId(ProductId id) {
        this.id = id;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return !(id != null ? !id.equals(product.id) : product.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
