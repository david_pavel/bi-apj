package cz.cvut.pavelda2.eshop.model.entity;

import java.io.Serializable;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public abstract class AbstractId<T extends AbstractId> implements Comparable<T>, Serializable{
    static final long serialVersionUID = 1L;

    protected Integer id;

    protected AbstractId() {}

    public AbstractId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object that) {
        if (that == null) return false;

        T thatId = (T) that;

        return compareTo(thatId) == 0;

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int compareTo(T that) {
        return this.id  - that.getId();
    }


    @Override
    public String toString() {
        return "" + id;
    }
}
