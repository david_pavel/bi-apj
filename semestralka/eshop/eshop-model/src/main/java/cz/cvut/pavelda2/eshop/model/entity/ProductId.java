package cz.cvut.pavelda2.eshop.model.entity;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class ProductId extends AbstractId<ProductId> {
    private ProductId() {
        super();
    }
    public ProductId(Integer id) {
        super(id);
    }

}
