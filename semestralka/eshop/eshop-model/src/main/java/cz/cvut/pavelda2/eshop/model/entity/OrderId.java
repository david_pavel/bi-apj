package cz.cvut.pavelda2.eshop.model.entity;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class OrderId extends AbstractId<OrderId> {
    private OrderId(){}
    public OrderId(Integer id) {
        super(id);
    }

}
