package cz.cvut.pavelda2.eshop.model;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class ModelActivator implements BundleActivator {
    private Logger log = Logger.getLogger(ModelActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        log.info(">>> Model module started <<<");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Model module stopped <<<");
    }
}
