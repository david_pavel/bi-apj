package cz.cvut.pavelda2.eshop.model.entity;

import java.io.Serializable;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class Customer implements Serializable {
    static final long serialVersionUID = 1L;

    private CustomerId id;
    private String name;

    public Customer() {
    }

    public Customer(CustomerId id, String name) {
        this.id = id;
        this.name = name;
    }
    public Customer(String name) {
        this(null, name);
    }

    public String getName() {
        return name;
    }

    public CustomerId getId() {
        return id;
    }

    public void setId(CustomerId id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return !(id != null ? !id.equals(customer.id) : customer.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
