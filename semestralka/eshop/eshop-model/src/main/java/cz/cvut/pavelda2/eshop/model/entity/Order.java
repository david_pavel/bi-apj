package cz.cvut.pavelda2.eshop.model.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class Order implements Serializable {
    static final long serialVersionUID = 1L;

    private OrderId id;
    private List<OrderItem> items;
    private Customer customer;

    public Order() {
    }

    public void setId(OrderId id) {
        this.id = id;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Order(OrderId id, List<OrderItem> items, Customer customer) {
        this.id = id;
        this.customer = customer;
        this.items = items;
    }

    public Order(List<OrderItem> orderItems, Customer customer) {
        this(null, orderItems, customer);
    }

    public OrderId getId() {
        return id;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return !(id != null ? !id.equals(order.id) : order.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", items=" + items +
                ", customer=" + customer +
                '}';
    }
}
