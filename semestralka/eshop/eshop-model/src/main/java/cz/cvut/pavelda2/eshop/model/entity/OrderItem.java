package cz.cvut.pavelda2.eshop.model.entity;

import java.io.Serializable;

/**
 * Created by David Pavel on 20. 10. 2015.
 */
public class OrderItem implements Serializable {
    static final long serialVersionUID = 1L;
    private Product product;
    private Integer count;

    public OrderItem(Product product, Integer count) {
        this.product = product;
        this.count = count;
    }

    public OrderItem() {
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setProduct(Product product) {

        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public Integer getCount() {
        return count;
    }


    public Double getPrice() {
        return product.getPrice() * count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderItem orderItem = (OrderItem) o;

        if (product != null ? !product.equals(orderItem.product) : orderItem.product != null) return false;
        return !(count != null ? !count.equals(orderItem.count) : orderItem.count != null);

    }

    @Override
    public int hashCode() {
        int result = product != null ? product.hashCode() : 0;
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "product=" + product +
                ", count=" + count +
                '}';
    }
}
