package cz.cvut.pavelda2.eshop.model.entity;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Created by David Pavel on 13. 10. 2015.
 */
public class ProductTest {

    @Test
    public void testGetId() throws Exception {
        System.out.println("getId");

        final Product product = new Product(new ProductId(2),"xxx",50.0);
        final ProductId expectedId = new ProductId(2);

        assertNotNull(product);
        assertEquals(expectedId,product.getId());

    }
}