package cz.cvut.pavelda2.eshop.utils;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by David Pavel on 9. 1. 2016.
 */
public class Marshaller {
    private static final Logger LOG = Logger.getLogger(Marshaller.class.getName());

    public static Object bytes2Object(byte[] bytes) {
        try {
            return new ObjectInputStream(new ByteArrayInputStream(bytes)).readObject();
        } catch (IOException | ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public static byte[] obj2Bytes(Object obj) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);) {
            oos.writeObject(obj);
            oos.flush();
            return baos.toByteArray();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
}
