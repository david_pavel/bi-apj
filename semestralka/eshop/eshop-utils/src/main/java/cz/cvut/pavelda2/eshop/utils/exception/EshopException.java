package cz.cvut.pavelda2.eshop.utils.exception;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class EshopException extends Exception {
    public EshopException() {
    }

    public EshopException(String message) {
        super(message);
    }

    public EshopException(String message, Throwable cause) {
        super(message, cause);
    }

    public EshopException(Throwable cause) {
        super(cause);
    }

    public EshopException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
