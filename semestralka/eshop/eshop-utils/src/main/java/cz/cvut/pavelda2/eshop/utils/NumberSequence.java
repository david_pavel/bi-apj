package cz.cvut.pavelda2.eshop.utils;

/**
 * Created by David Pavel on 15. 12. 2015.
 */
public class NumberSequence {
    private int sequenceValue;

    public NumberSequence() {
        sequenceValue = 1;
    }

    public NumberSequence(int initialNumber) {
        this.sequenceValue = initialNumber;
    }

    public int next() {
        return sequenceValue++;
    }
}
