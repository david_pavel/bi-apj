package cz.cvut.pavelda2.eshop.utils;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class UtilsActivator implements BundleActivator {
    private Logger log = Logger.getLogger(UtilsActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        log.info(">>> Utils module started <<<");

    }


    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Utils module stopped <<<");
    }
}
