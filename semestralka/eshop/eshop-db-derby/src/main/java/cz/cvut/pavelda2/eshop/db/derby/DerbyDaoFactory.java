package cz.cvut.pavelda2.eshop.db.derby;

import java.sql.*;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class DerbyDaoFactory extends DaoFactory {
    private Logger LOG = Logger.getLogger(DerbyDaoFactory.class.getName());

    private static final String CONNECTION_STRING = "jdbc:derby:"+System.getProperty("user.home")+"/derby/apj-eshop;create=true";

    final Connection connection;

    private final ProductDao productDaoInstance;

    public DerbyDaoFactory() throws EshopException {
        try {
            DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
            connection = DriverManager.getConnection(CONNECTION_STRING);

            try {
                executeDDL();
            } catch (SQLException ex) {
                LOG.warning("DDL failed: " + ex.toString());
            }

            // Init DAOs
            productDaoInstance = new DerbyProductDao(connection);

        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

    private void executeDDL() throws SQLException {
        DatabaseMetaData dbmd = connection.getMetaData();
        ResultSet rs = dbmd.getTables(null, null, DerbyProductDao.T_PRODUCT, null);
        if (rs.next() == false) { // table not exists
            LOG.info("Creating new table.");
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + DerbyProductDao.T_PRODUCT
                    + "(ID INT NOT NULL GENERATED ALWAYS AS IDENTITY,"
                    + "name   VARCHAR(50),"
                    + "price    DOUBLE PRECISION,"
                    + "PRIMARY KEY (ID))");
        } else {
            LOG.info("Table already exists.");
        }
    }

    @Override
    public ProductDao getProductDao() throws EshopException {
        throw new EshopException("Not implemented in derby");
    }

    @Override
    public CustomerDao getCustomerDao() throws EshopException {
        throw new EshopException("Not implemented in derby");
    }

    @Override
    public OrderDao getOrderDao() throws EshopException {
        throw new EshopException("Not implemented in derby");
    }
}
