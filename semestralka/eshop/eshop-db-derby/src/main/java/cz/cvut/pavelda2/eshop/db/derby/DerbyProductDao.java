package cz.cvut.pavelda2.eshop.db.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class DerbyProductDao implements ProductDao {
    private static final Logger logger = Logger.getLogger(DerbyProductDao.class.getName());

    public static final String T_PRODUCT = "t_product";

    private PreparedStatement psCreateBooks;
    private PreparedStatement psGetAll;

    public DerbyProductDao(Connection connection)  {
        try {
            psGetAll = connection.prepareStatement("SELECT * FROM " + T_PRODUCT);
            psCreateBooks = connection.prepareStatement("INSERT INTO " + T_PRODUCT + " VALUES (DEFAULT, ?, ?)");
        } catch (SQLException e) {
            logger.warning(e.toString());
        }
    }

    @Override
    public Product create(Product entity) throws EshopException {
        try {
            psCreateBooks.setString(1, entity.getName());
            psCreateBooks.setDouble(2, entity.getPrice());
            psCreateBooks.executeUpdate();
        } catch (SQLException e) {
            throw new EshopException(e);
        }
        return null; // TODO: find it
    }

    @Override
    public Product update(Product entity) throws EshopException {
        return null;
    }

    @Override
    public void remove(ProductId entityId) throws EshopException {

    }

    @Override
    public List<Product> findAll() throws EshopException {
        try {
            List<Product> products = new ArrayList<>();
            ResultSet rs = psGetAll.executeQuery();
            while(rs.next()) {
                products.add(new Product(new ProductId(rs.getInt("id")), rs.getString("name"), rs.getDouble("price")));
            }
            return products;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

    @Override
    public Product findById(ProductId entityId) throws EshopException {
        return null;
    }
}
