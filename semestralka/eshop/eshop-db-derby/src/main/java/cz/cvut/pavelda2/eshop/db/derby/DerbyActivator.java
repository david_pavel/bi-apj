package cz.cvut.pavelda2.eshop.db.derby;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class DerbyActivator implements BundleActivator {
    private Logger log = Logger.getLogger(DerbyActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        log.info(">>> Derby module started <<<");
        bundleContext.registerService(DaoFactory.class.getName(), new DerbyDaoFactory(), null);
    }


    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Derby module stopped <<<");
    }
}
