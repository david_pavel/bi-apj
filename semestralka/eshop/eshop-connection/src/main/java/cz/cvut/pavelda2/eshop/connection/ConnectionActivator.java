package cz.cvut.pavelda2.eshop.connection;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.business.facade.EshopFacade;
import cz.cvut.pavelda2.eshop.connection.ui.action.CreateConnectionAction;
import cz.cvut.pavelda2.eshop.connection.ui.menu.ConnectionMenu;
import cz.cvut.pavelda2.eshop.connection.ui.action.DisconnectAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.ActionState;
import cz.cvut.pavelda2.eshop.richclient.ui.component.window.MainWindow;
import javafx.application.Platform;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class ConnectionActivator implements BundleActivator {
    private Logger log = Logger.getLogger(ConnectionActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Connection module started <<<");

        ServiceTracker<AbstractConnection, AbstractConnection> st = new ServiceTracker<>(bundleContext, AbstractConnection.class, null);
        st.open();
        AbstractConnection.setServiceTracker(st);

        // Add connection menu
        Platform.runLater(() -> {
            MainWindow.instance.getMenuBar().getMenus().addAll(new ConnectionMenu());
            MainWindow.instance.getToolBar().getItems().addAll(
                    CreateConnectionAction.instance.createButton(),
                    DisconnectAction.instance.createButton());
            ActionState.instance.fireUpdate();
        });

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        Platform.runLater(() -> AbstractConnection.getService().disconnect());
        log.info(">>> Connection module stopped <<<");
    }
}
