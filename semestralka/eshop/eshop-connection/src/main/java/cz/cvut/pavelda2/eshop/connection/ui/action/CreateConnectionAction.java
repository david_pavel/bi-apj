package cz.cvut.pavelda2.eshop.connection.ui.action;


import java.io.InputStream;

import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import cz.cvut.pavelda2.eshop.connection.DefaultConnection;
import cz.cvut.pavelda2.eshop.connection.ui.dialog.CreateConnectionDialog;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class CreateConnectionAction extends AbstractAction {
    public static AbstractAction instance = new CreateConnectionAction();

    private int port;
    private String host;

    public CreateConnectionAction() {
        super(Messages.CONNECT_ACTION.createMsg(), KeyCombination.keyCombination("CTRL+ALT+C"));
    }

    @Override
    public void execute() {
        CreateConnectionDialog createConnectionDialog = new CreateConnectionDialog();
        createConnectionDialog.execute();
    }

    @Override
    public boolean isDisabled() {
        return AbstractConnection.getService().isConnected();
    }

    @Override
    public Button createButton() {
        Button button = super.createButton();
        InputStream imageStream = getClass().getClassLoader().getResourceAsStream("images/button_connect_2.png");
        if (imageStream!= null) {
            button.setGraphic(new ImageView(new Image(imageStream)));
            button.setTooltip(new Tooltip(button.getText()));
            button.setText("");
        }
        return button;
    }
}
