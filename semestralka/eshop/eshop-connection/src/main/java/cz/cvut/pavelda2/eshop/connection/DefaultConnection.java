package cz.cvut.pavelda2.eshop.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.protocol.command.LogoutCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class DefaultConnection extends AbstractConnection {
    protected static final Logger LOG = Logger.getLogger(DefaultConnection.class.getName());
    protected static final int SOCKET_TIMEOUT_MS = 3000;

    protected ObjectOutputStream oos;
    protected ObjectInputStream ois;
    protected Socket socket;

    @Override
    public void connect(String host, int port) throws EshopException {
        try {
            LOG.info("Creating connection to " + host + ":" + port);
            socket = new Socket(host, port);
            socket.setSoTimeout(SOCKET_TIMEOUT_MS);
            LOG.info("Init input stream");
            oos = new ObjectOutputStream(socket.getOutputStream());
            LOG.info("Init output stream");
            ois = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            throw new EshopException(e); // Business error
        }
    }

    @Override
    public void disconnect() {
        LOG.info("Disconnecting from server...");
        try (
                ObjectOutputStream oos = this.oos;
                ObjectInputStream ois = this.ois;
                Socket s = this.socket) {
            send(LogoutCommand.getInstance());
        } catch (IOException | EshopException e) {
        } finally {
            socket = null;
        }
    }

    @Override
    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    @Override
    public <T> T send(AbstractCommand command) throws EshopException {

        LOG.info("Sending command: " + command.toString());
        if (!isConnected()) {
            throw new EshopException("Can not send command " + command.getClass().getSimpleName() + ". Not connected to a Server.");
        }
        try {
            oos.writeObject(command);
            oos.flush();
            Object result = ois.readObject();
            if (result instanceof Exception) {
                throw (EshopException) result;
            }

            return (T) result;
        } catch (IOException e) {
            throw new EshopException(e); // Business error
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e); // Application error (Some class on client missing)
        }
    }
}
