package cz.cvut.pavelda2.eshop.connection;

import java.util.Arrays;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.protocol.command.AbstractCommand;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Created by David Pavel on 22. 12. 2015.
 */
public abstract class AbstractConnection {
    private static final Logger LOG = Logger.getLogger(AbstractConnection.class.getName());
    public static AbstractConnection service;

    public static AbstractConnection getService() {
        if (service == null) {
            service = serviceTracker.getService();
            if (service == null) {
                service = new DefaultConnection();
            }
            LOG.info("Using connection: " + service.getClass());
        }

        return service;
    }

    private static ServiceTracker<AbstractConnection, AbstractConnection> serviceTracker;

    public static void setServiceTracker(ServiceTracker<AbstractConnection, AbstractConnection> serviceTracker) {
        AbstractConnection.serviceTracker = serviceTracker;
    }

    public abstract void connect(String host, int port) throws EshopException;

    public abstract void disconnect();

    public abstract boolean isConnected();

    public abstract <T> T send(AbstractCommand command) throws EshopException;
}
