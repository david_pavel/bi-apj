package cz.cvut.pavelda2.eshop.connection.ui.menu;

import cz.cvut.pavelda2.eshop.connection.ui.action.CreateConnectionAction;
import cz.cvut.pavelda2.eshop.connection.ui.action.DisconnectAction;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import javafx.scene.control.Menu;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class ConnectionMenu extends Menu {
    public ConnectionMenu() {
        super(Messages.CONNECTION.createMsg());
        getItems().addAll(
                CreateConnectionAction.instance.createMenuItem(),
                DisconnectAction.instance.createMenuItem()
        );
    }
}
