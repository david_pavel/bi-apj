package cz.cvut.pavelda2.eshop.connection.ui.action;


import java.io.InputStream;

import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import cz.cvut.pavelda2.eshop.connection.DefaultConnection;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.AbstractAction;
import cz.cvut.pavelda2.eshop.richclient.ui.action.ActionState;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;

/**
 * Created by David Pavel on 8. 12. 2015.
 */
public class DisconnectAction extends AbstractAction {
    public static AbstractAction instance = new DisconnectAction();

    private int port;
    private String host;

    public DisconnectAction() {
        super(Messages.DISCONNECT_ACTION.createMsg(), KeyCombination.keyCombination("CTRL+ALT+D"));
    }

    @Override
    public void execute() {
        AbstractConnection.getService().disconnect();
        ActionState.instance.fireUpdate();
    }

    @Override
    public boolean isDisabled() {
        return !AbstractConnection.getService().isConnected();
    }

    @Override
    public Button createButton() {
        Button button = super.createButton();
        InputStream imageStream = getClass().getClassLoader().getResourceAsStream("images/button_disconnect_2.png");
        if (imageStream!= null) {
            button.setGraphic(new ImageView(new Image(imageStream)));
            button.setTooltip(new Tooltip(button.getText()));
            button.setText("");
        }
        return button;
    }
}
