package cz.cvut.pavelda2.eshop.connection.ui.dialog;


import cz.cvut.pavelda2.eshop.connection.AbstractConnection;
import cz.cvut.pavelda2.eshop.connection.DefaultConnection;
import cz.cvut.pavelda2.eshop.localisation.Messages;
import cz.cvut.pavelda2.eshop.richclient.ui.action.ActionState;
import cz.cvut.pavelda2.eshop.richclient.ui.component.alert.EshopAlert;
import cz.cvut.pavelda2.eshop.richclient.ui.component.dialog.AbstractCreateDialog;
import cz.cvut.pavelda2.eshop.richclient.ui.observable.EshopObservableManager;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public class CreateConnectionDialog extends AbstractCreateDialog {

    private static final String DEFAULT_HOST = "localhost";
    private static final Integer DEFAULT_PORT = 1234;
    private TextField host;
    private TextField port;

    public CreateConnectionDialog() {
        setTitle(Messages.CONNECTION_CREATE_DIALOG_TITLE.createMsg());
        this.initModality(Modality.APPLICATION_MODAL);
        this.getDialogPane().setContent(createInterior());

        ButtonType buttonTypeOk = new ButtonType(Messages.BUTTON_CONNECT.createMsg(), ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, buttonTypeOk);
    }

    @Override
    protected Node createInterior() {

        host = new TextField(DEFAULT_HOST);
        port = new TextField(DEFAULT_PORT.toString());

        GridPane grid = new GridPane();
        grid.add(new Label(Messages.CONNECTION_HOST.createMsg() + ": "), 1, 1);
        grid.add(host, 2, 1);
        grid.add(new Label(Messages.CONNECTION_PORT.createMsg() + ": "), 1, 2);
        grid.add(port, 2, 2);
        return grid;
    }

    @Override
    protected void onSubmit(ButtonType buttonType) {
        System.out.println("Connection URL: " + host.getText() + ":" + port.getText());
        try {
            AbstractConnection.getService().connect(host.getText(), Integer.parseInt(port.getText()));
        } catch (EshopException e) {
            EshopAlert.error(e);
        } finally {
            ActionState.instance.fireUpdate();
            EshopObservableManager.instance.notifyAllObservebles();
        }

    }
}
