package cz.cvut.pavelda2.eshop.localisation;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class LocalisationActivator implements BundleActivator {
    private Logger log = Logger.getLogger(LocalisationActivator.class.getName());

    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        log.info(">>> Localisation module started <<<");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Localisation module stopped <<<");

    }
}
