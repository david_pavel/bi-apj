package cz.cvut.pavelda2.eshop.localisation;

import javax.annotation.processing.Completion;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by David Pavel on 27. 10. 2015.
 */
public enum Messages {
    // Common
    APPLICATION_NAME,
    ID,
    PRICE,
    BUTTON_CREATE,
    BUTTON_SAVE,
    BUTTON_CONNECT,
    EXIT,

    // Exception
    UNEXPECTED_PROBLEM_ENCOUNTRED,

    // Menu
    MENU_FILE,
    MENU_PRODUCTS,
    MENU_CUSTOMERS,

    // Product
    PRODUCT_ADD,
    PRODUCT_DELETE,
    PRODUCT_UPDATE,
    PRODUCTS_LIST,
    PRODUCT_NAME,
    PRODUCT_PRICE,
    PRODUCT_UPDATE_DIALOG_TITLE,
    PRODUCT_CREATE_DIALOG_TITLE,
    PRODUCT,

    // Customer
    CUSTOMER_CREATE_DIALOG_TITLE,
    CUSTOMERS_LIST,
    CUSTOMER_NAME,
    CUSTOMER_ADD,
    CUSTOMER_DELETE,
    CUSTOMER_UPDATE,
    CUSTOMER,
    CUSTOMER_UPDATE_DIALOG_TITLE,

    // Order
    ORDERS_LIST,
    ORDER_DELETE,
    MENU_ORDERS,
    ORDER_ADD,
    ORDER_UPDATE,
    ORDER_CREATE_DIALOG_TITLE,
    ADD_ORDER_ITEM_DIALOG_TITLE,

    // Connection
    CONNECT_ACTION,
    DISCONNECT_ACTION,
    CONNECTION,
    CONNECTION_CREATE_DIALOG_TITLE,
    CONNECTION_HOST,
    CONNECTION_PORT, PRODUCTS_COUNT, TOTAL_PRICE;





    private static final String DEFAULT_LOCALISATION_BUNDLE = "cz.cvut.pavelda2.eshop.localisation.messages";

    public String createMsg(Object... params) {
        ResourceBundle rs = ResourceBundle.getBundle(DEFAULT_LOCALISATION_BUNDLE);
        try {
            return MessageFormat.format(rs.getString(name()), params);
        } catch (RuntimeException ex) {
            Logger.getLogger(Messages.class.getName()).info("Missing message: " + name());
            return toHumanReadableString();
        }
    }

    private String toHumanReadableString() {
        StringBuilder sb = new StringBuilder();
        boolean toUpper = true;
        for (Character c : name().toCharArray()) {
            switch (c) {
                case '_':
                    sb.append(" ");
                    toUpper = true;
                    break;
                default:
                    sb.append(toUpper ? Character.toUpperCase(c) : Character.toLowerCase(c));
                    toUpper = false;
            }
        }
        return sb.toString();
    }
}
