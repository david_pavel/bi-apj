--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.13
-- Dumped by pg_dump version 9.2.13
-- Started on 2015-12-08 23:30:41

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE "apj-eshop";
--
-- TOC entry 1955 (class 1262 OID 52504)
-- Name: apj-eshop; Type: DATABASE; Schema: -; Owner: apj-eshop
--

CREATE DATABASE "apj-eshop" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Czech_Czech Republic.1250' LC_CTYPE = 'Czech_Czech Republic.1250';


ALTER DATABASE "apj-eshop" OWNER TO "apj-eshop";

\connect "apj-eshop"

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 1956 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 175 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1958 (class 0 OID 0)
-- Dependencies: 175
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 170 (class 1259 OID 52525)
-- Name: t_customer_seq; Type: SEQUENCE; Schema: public; Owner: apj-eshop
--

CREATE SEQUENCE t_customer_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_customer_seq OWNER TO "apj-eshop";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 52527)
-- Name: t_customer; Type: TABLE; Schema: public; Owner: apj-eshop; Tablespace: 
--

CREATE TABLE t_customer (
    id integer DEFAULT nextval('t_customer_seq'::regclass) NOT NULL,
    name character varying(50)
);


ALTER TABLE public.t_customer OWNER TO "apj-eshop";

--
-- TOC entry 172 (class 1259 OID 52533)
-- Name: t_order_seq; Type: SEQUENCE; Schema: public; Owner: apj-eshop
--

CREATE SEQUENCE t_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_order_seq OWNER TO "apj-eshop";

--
-- TOC entry 173 (class 1259 OID 52535)
-- Name: t_order; Type: TABLE; Schema: public; Owner: apj-eshop; Tablespace: 
--

CREATE TABLE t_order (
    id integer DEFAULT nextval('t_order_seq'::regclass) NOT NULL,
    customer_id integer NOT NULL
);


ALTER TABLE public.t_order OWNER TO "apj-eshop";

--
-- TOC entry 174 (class 1259 OID 52546)
-- Name: t_order_item; Type: TABLE; Schema: public; Owner: apj-eshop; Tablespace: 
--

CREATE TABLE t_order_item (
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    count integer NOT NULL
);


ALTER TABLE public.t_order_item OWNER TO "apj-eshop";

--
-- TOC entry 168 (class 1259 OID 52517)
-- Name: t_product_seq; Type: SEQUENCE; Schema: public; Owner: apj-eshop
--

CREATE SEQUENCE t_product_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_product_seq OWNER TO "apj-eshop";

--
-- TOC entry 169 (class 1259 OID 52519)
-- Name: t_product; Type: TABLE; Schema: public; Owner: apj-eshop; Tablespace: 
--

CREATE TABLE t_product (
    id integer DEFAULT nextval('t_product_seq'::regclass) NOT NULL,
    name character varying(50),
    price double precision
);


ALTER TABLE public.t_product OWNER TO "apj-eshop";

--
-- TOC entry 1947 (class 0 OID 52527)
-- Dependencies: 171
-- Data for Name: t_customer; Type: TABLE DATA; Schema: public; Owner: apj-eshop
--

INSERT INTO t_customer (id, name) VALUES (1, 'David');
INSERT INTO t_customer (id, name) VALUES (3, 'aaa');


--
-- TOC entry 1959 (class 0 OID 0)
-- Dependencies: 170
-- Name: t_customer_seq; Type: SEQUENCE SET; Schema: public; Owner: apj-eshop
--

SELECT pg_catalog.setval('t_customer_seq', 4, true);


--
-- TOC entry 1949 (class 0 OID 52535)
-- Dependencies: 173
-- Data for Name: t_order; Type: TABLE DATA; Schema: public; Owner: apj-eshop
--

INSERT INTO t_order (id, customer_id) VALUES (6, 1);
INSERT INTO t_order (id, customer_id) VALUES (7, 1);
INSERT INTO t_order (id, customer_id) VALUES (8, 1);
INSERT INTO t_order (id, customer_id) VALUES (9, 1);
INSERT INTO t_order (id, customer_id) VALUES (10, 1);
INSERT INTO t_order (id, customer_id) VALUES (11, 1);
INSERT INTO t_order (id, customer_id) VALUES (12, 1);
INSERT INTO t_order (id, customer_id) VALUES (13, 1);
INSERT INTO t_order (id, customer_id) VALUES (17, 1);
INSERT INTO t_order (id, customer_id) VALUES (1, 1);
INSERT INTO t_order (id, customer_id) VALUES (19, 1);
INSERT INTO t_order (id, customer_id) VALUES (21, 1);


--
-- TOC entry 1950 (class 0 OID 52546)
-- Dependencies: 174
-- Data for Name: t_order_item; Type: TABLE DATA; Schema: public; Owner: apj-eshop
--

INSERT INTO t_order_item (order_id, product_id, count) VALUES (7, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (7, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (7, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (8, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (8, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (8, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (9, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (9, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (9, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (10, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (10, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (10, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (11, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (11, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (11, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (12, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (12, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (12, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (13, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (13, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (13, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (17, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (17, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (17, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (1, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (1, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (1, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (19, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (19, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (19, 3, 3);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (21, 1, 1);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (21, 2, 5);
INSERT INTO t_order_item (order_id, product_id, count) VALUES (21, 3, 3);


--
-- TOC entry 1960 (class 0 OID 0)
-- Dependencies: 172
-- Name: t_order_seq; Type: SEQUENCE SET; Schema: public; Owner: apj-eshop
--

SELECT pg_catalog.setval('t_order_seq', 22, true);


--
-- TOC entry 1945 (class 0 OID 52519)
-- Dependencies: 169
-- Data for Name: t_product; Type: TABLE DATA; Schema: public; Owner: apj-eshop
--

INSERT INTO t_product (id, name, price) VALUES (1, 'Košík', 129.90000000000001);
INSERT INTO t_product (id, name, price) VALUES (2, 'Příbory', 350);
INSERT INTO t_product (id, name, price) VALUES (3, 'Šátek', 300);
INSERT INTO t_product (id, name, price) VALUES (6, 'Bon bon', 1);
INSERT INTO t_product (id, name, price) VALUES (10, 'Test', 123);
INSERT INTO t_product (id, name, price) VALUES (12, 'aaaa', 123);


--
-- TOC entry 1961 (class 0 OID 0)
-- Dependencies: 168
-- Name: t_product_seq; Type: SEQUENCE SET; Schema: public; Owner: apj-eshop
--

SELECT pg_catalog.setval('t_product_seq', 12, true);


--
-- TOC entry 1832 (class 2606 OID 52532)
-- Name: t_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: apj-eshop; Tablespace: 
--

ALTER TABLE ONLY t_customer
    ADD CONSTRAINT t_customer_pkey PRIMARY KEY (id);


--
-- TOC entry 1834 (class 2606 OID 52540)
-- Name: t_order_pkey; Type: CONSTRAINT; Schema: public; Owner: apj-eshop; Tablespace: 
--

ALTER TABLE ONLY t_order
    ADD CONSTRAINT t_order_pkey PRIMARY KEY (id);


--
-- TOC entry 1830 (class 2606 OID 52524)
-- Name: t_product_pkey; Type: CONSTRAINT; Schema: public; Owner: apj-eshop; Tablespace: 
--

ALTER TABLE ONLY t_product
    ADD CONSTRAINT t_product_pkey PRIMARY KEY (id);


--
-- TOC entry 1835 (class 2606 OID 52541)
-- Name: t_order_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: apj-eshop
--

ALTER TABLE ONLY t_order
    ADD CONSTRAINT t_order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES t_customer(id);


--
-- TOC entry 1836 (class 2606 OID 52549)
-- Name: t_order_item_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: apj-eshop
--

ALTER TABLE ONLY t_order_item
    ADD CONSTRAINT t_order_item_order_id_fkey FOREIGN KEY (order_id) REFERENCES t_order(id);


--
-- TOC entry 1837 (class 2606 OID 52554)
-- Name: t_order_item_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: apj-eshop
--

ALTER TABLE ONLY t_order_item
    ADD CONSTRAINT t_order_item_product_id_fkey FOREIGN KEY (product_id) REFERENCES t_product(id);


--
-- TOC entry 1957 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-12-08 23:30:41

--
-- PostgreSQL database dump complete
--
