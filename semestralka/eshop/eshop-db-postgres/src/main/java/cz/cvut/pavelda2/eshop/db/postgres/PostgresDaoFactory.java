package cz.cvut.pavelda2.eshop.db.postgres;

import static cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresCustomerDao.T_CUSTOMER;
import static cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresOrderDao.T_ORDER;
import static cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresOrderDao.T_ORDER_ITEM;
import static cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresProductDao.T_PRODUCT;

import java.sql.*;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresCustomerDao;
import cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresOrderDao;
import cz.cvut.pavelda2.eshop.db.postgres.dao.PostgresProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class PostgresDaoFactory extends DaoFactory {
    private static final Logger LOG = Logger.getLogger(PostgresDaoFactory.class.getName());

    private static final String T_PRODUCT_SEQ = T_PRODUCT + "_seq";
    private static final String T_CUSTOMER_SEQ = T_CUSTOMER + "_seq";
    private static final String T_ORDER_SEQ = T_ORDER + "_seq";
    private static final String SCHEMA = "public";
    private static final String CONNECTION_STRING = "jdbc:postgresql://localhost:5432/apj-eshop?user=apj-eshop&password=apj-eshop";

    private final Connection conn;
    private final ProductDao productDaoInstance;
    private final CustomerDao customerDaoInstance;
    private final OrderDao orderDaoInstance;

    public PostgresDaoFactory() throws EshopException {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            conn = DriverManager.getConnection(CONNECTION_STRING);

            conn.setAutoCommit(false);

            try {
                executeDDL();
            } catch (SQLException ex) {
                LOG.warning("DDL failed: " + ex.toString());
            }

            // Init DAOs
            productDaoInstance = new PostgresProductDao(conn);
            customerDaoInstance = new PostgresCustomerDao(conn);
            orderDaoInstance = new PostgresOrderDao(conn, customerDaoInstance, productDaoInstance);

        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

    private void executeDDL() throws SQLException {
        if (!tableExists(T_PRODUCT, SCHEMA)) { // table not exists
            LOG.info("Creating new table: " + T_PRODUCT);
            conn.createStatement().executeUpdate("CREATE SEQUENCE " + T_PRODUCT_SEQ);
            conn.createStatement().executeUpdate("CREATE TABLE " + T_PRODUCT
                    + "(id INT NOT NULL DEFAULT NEXTVAL('" + T_PRODUCT_SEQ + "'),"
                    + "name   VARCHAR(50),"
                    + "price    DOUBLE PRECISION,"
                    + "PRIMARY KEY (id))");
            conn.commit();
        } else {
            LOG.info("Table already exists: " + T_PRODUCT);
        }

        if (!tableExists(T_CUSTOMER, SCHEMA)) { // table not exists
            LOG.info("Creating new table: " + T_CUSTOMER);
            conn.createStatement().executeUpdate("CREATE SEQUENCE " + T_CUSTOMER_SEQ);
            conn.createStatement().executeUpdate("CREATE TABLE " + T_CUSTOMER
                    + "(id INT NOT NULL DEFAULT NEXTVAL('" + T_CUSTOMER_SEQ + "'),"
                    + "name   VARCHAR(50),"
                    + "PRIMARY KEY (id))");
            conn.commit();
        } else {
            LOG.info("Table already exists: " + T_CUSTOMER);
        }

        if (!tableExists(T_ORDER, SCHEMA)) { // table not exists
            LOG.info("Creating new table: " + T_ORDER);
            conn.createStatement().executeUpdate("CREATE SEQUENCE " + T_ORDER_SEQ);
            conn.createStatement().executeUpdate("CREATE TABLE " + T_ORDER
                    + "(id INT NOT NULL DEFAULT NEXTVAL('" + T_ORDER_SEQ + "'),"
                    + "customer_id INT NOT NULL REFERENCES " + T_CUSTOMER + "(id),"
                    + "PRIMARY KEY (id))");
            conn.commit();
        } else {
            LOG.info("Table already exists: " + T_ORDER);
        }

        if (!tableExists(T_ORDER_ITEM, SCHEMA)) { // table not exists
            LOG.info("Creating new table: " + T_ORDER_ITEM);
            conn.createStatement().executeUpdate("CREATE TABLE " + T_ORDER_ITEM
                    + "(order_id INT NOT NULL REFERENCES " + T_ORDER + "(id),"
                    + "product_id INT NOT NULL REFERENCES " + T_PRODUCT + "(id),"
                    + "count INT NOT NULL)");
            conn.commit();
        } else {
            LOG.info("Table already exists: " + T_ORDER_ITEM);
        }
    }

    private boolean tableExists(String tableName, String schemaName) throws SQLException {
        DatabaseMetaData metadata = conn.getMetaData();
        ResultSet resultSet = metadata.getTables(null, schemaName, tableName, null);
        return resultSet.next();
    }

    @Override
    public ProductDao getProductDao() throws EshopException {
        return productDaoInstance;
    }

    @Override
    public CustomerDao getCustomerDao() throws EshopException {
        return customerDaoInstance;
    }

    @Override
    public OrderDao getOrderDao() throws EshopException {
        return orderDaoInstance;
    }
}
