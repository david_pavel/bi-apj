package cz.cvut.pavelda2.eshop.db.postgres.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.model.entity.Product;
import cz.cvut.pavelda2.eshop.model.entity.ProductId;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;
import org.postgresql.util.PSQLException;

/**
 * Created by David Pavel on 24. 11. 2015.
 */
public class PostgresProductDao extends AbstractPostgresDao implements ProductDao {
    private static final Logger logger = Logger.getLogger(PostgresProductDao.class.getName());

    public static final String T_PRODUCT = "t_product";
    private static final String T_COLUMNS = String.join(", ", new String[]{"id", "name", "price"});


    private PreparedStatement psCreate;
    private PreparedStatement psFindAll;
    private PreparedStatement psUpdate;
    private PreparedStatement psFindById;
    private PreparedStatement psRemove;

    public PostgresProductDao(Connection connection)  {
        super(connection);
        try {
            psFindAll = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_PRODUCT);
            psFindById = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_PRODUCT + " WHERE id = ? ");
            psCreate = connection.prepareStatement("INSERT INTO " + T_PRODUCT + " VALUES (DEFAULT, ?, ?)");
            psUpdate = connection.prepareStatement("UPDATE " + T_PRODUCT + " SET name = ?, price = ? WHERE id = ? ");
            psRemove = connection.prepareStatement("DELETE FROM " + T_PRODUCT + " WHERE id = ? ");
        } catch (SQLException e) {
            logger.warning(e.toString());
        }
    }

    @Override
    public Product create(Product entity) throws EshopException {
        if (entity.getId() != null && entity.getId().getId() != null) {
            throw new EshopException("Product id must be null when creating new product.");
        }
        try {
            psCreate.setString(1, entity.getName());
            psCreate.setDouble(2, entity.getPrice());
            psCreate.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
        return null; // TODO: find it
    }

    @Override
    public Product update(Product entity) throws EshopException {
        if (entity.getId() == null || entity.getId().getId() == null) {
            throw new EshopException("Product id can not be null.");
        }
        try {
            psUpdate.setString(1, entity.getName());
            psUpdate.setDouble(2, entity.getPrice());
            psUpdate.setInt(3, entity.getId().getId());
            psUpdate.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
        return null; // TODO: find it
    }

    @Override
    public void remove(ProductId entityId) throws EshopException {
        try {
            psRemove.setInt(1, entityId.getId());
            psRemove.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
    }

    @Override
    public List<Product> findAll() throws EshopException {
        try {
            List<Product> products = new ArrayList<>();
            ResultSet rs = psFindAll.executeQuery();
            while(rs.next()) {
                products.add(new Product(new ProductId(rs.getInt("id")), rs.getString("name"), rs.getDouble("price")));
            }
            return products;
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
    }

    @Override
    public Product findById(ProductId entityId) throws EshopException {
        try {
            psFindById.setInt(1, entityId.getId());
            ResultSet rs = psFindById.executeQuery();
            if (!rs.next()) {
                return null; // entity not found
            }
            Product product = new Product(new ProductId(rs.getInt("id")), rs.getString("name"), rs.getDouble("price"));

            if (rs.next()) {
                throw new EshopException("Multiple products found when searching by id '" + entityId.toString() + "'");
            }

            return product;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }


}
