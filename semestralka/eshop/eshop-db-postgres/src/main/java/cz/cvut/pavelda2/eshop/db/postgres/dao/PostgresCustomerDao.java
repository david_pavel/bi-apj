package cz.cvut.pavelda2.eshop.db.postgres.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.model.entity.Customer;
import cz.cvut.pavelda2.eshop.model.entity.CustomerId;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 3. 12. 2015.
 */
public class PostgresCustomerDao extends AbstractPostgresDao implements CustomerDao {
    public static final String T_CUSTOMER = "t_customer";
    private static final Logger logger = Logger.getLogger(PostgresCustomerDao.class.getName());
    private static final String T_COLUMNS = String.join(", ", new String[]{"id", "name"});

    private PreparedStatement psCreate;
    private PreparedStatement psFindAll;
    private PreparedStatement psUpdate;
    private PreparedStatement psFindById;
    private PreparedStatement psRemove;

    public PostgresCustomerDao(Connection connection) {
        super(connection);
        try {
            psFindAll = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_CUSTOMER);
            psFindById = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_CUSTOMER + " WHERE id = ? ");
            psCreate = connection.prepareStatement("INSERT INTO " + T_CUSTOMER + " VALUES (DEFAULT, ?)");
            psUpdate = connection.prepareStatement("UPDATE " + T_CUSTOMER + " SET name = ? WHERE id = ? ");
            psRemove = connection.prepareStatement("DELETE FROM " + T_CUSTOMER + " WHERE id = ? ");
        } catch (SQLException e) {
            logger.warning(e.toString());
        }
    }

    @Override
    public Customer create(Customer entity) throws EshopException {
        if (entity.getId() != null && entity.getId().getId() != null) {
            throw new EshopException("Customer id must be null when creating new Customer.");
        }
        try {
            psCreate.setString(1, entity.getName());
            psCreate.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
        return null; // TODO: find it
    }

    @Override
    public Customer update(Customer entity) throws EshopException {
        if (entity.getId() == null || entity.getId().getId() == null) {
            throw new EshopException("Customer id can not be null.");
        }
        try {
            psUpdate.setString(1, entity.getName());
            psUpdate.setInt(2, entity.getId().getId());
            psUpdate.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }

        return null; // TODO: find it
    }

    @Override
    public void remove(CustomerId entityId) throws EshopException {
        try {
            psRemove.setInt(1, entityId.getId());
            psRemove.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
    }

    @Override
    public List<Customer> findAll() throws EshopException {
        try {
            List<Customer> Customers = new ArrayList<>();
            ResultSet rs = psFindAll.executeQuery();
            while (rs.next()) {
                Customers.add(new Customer(new CustomerId(rs.getInt("id")), rs.getString("name")));
            }
            return Customers;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

    @Override
    public Customer findById(CustomerId entityId) throws EshopException {
        try {
            psFindById.setInt(1, entityId.getId());
            ResultSet rs = psFindById.executeQuery();
            if (!rs.next()) {
                return null; // entity not found
            }
            Customer Customer = new Customer(new CustomerId(rs.getInt("id")), rs.getString("name"));

            if (rs.next()) {
                throw new EshopException("Multiple Customers found when searching by id '" + entityId.toString() + "'");
            }

            return Customer;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

}
