package cz.cvut.pavelda2.eshop.db.postgres;

import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.service.DaoFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by David Pavel on 6. 10. 2015.
 */
public class PostgresActivator implements BundleActivator {
    private Logger log = Logger.getLogger(PostgresActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        log.info(">>> Postgres module started <<<");
        bundleContext.registerService(DaoFactory.class.getName(), new PostgresDaoFactory(), null);
    }


    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        log.info(">>> Postgres module stopped <<<");
    }
}
