package cz.cvut.pavelda2.eshop.db.postgres.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import cz.cvut.pavelda2.eshop.integration.dao.ProductDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.CustomerDao;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.model.entity.*;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 3. 12. 2015.
 */
public class PostgresOrderDao extends AbstractPostgresDao implements OrderDao {
    public static final String T_ORDER = "t_order";
    public static final String T_ORDER_ITEM = "t_order_item";
    private static final Logger logger = Logger.getLogger(PostgresOrderDao.class.getName());
    private static final String T_COLUMNS = String.join(", ", new String[]{"id", "customer_id"});
    private static final String T_ORDER_ITEM_COLUMNS = String.join(", ", new String[]{"order_id", "product_id", "count"});
    private final CustomerDao customerDao;
    private final ProductDao productDao;

    private PreparedStatement psCreate;
    private PreparedStatement psCreateOrderItem;
    private PreparedStatement psFindAll;
    private PreparedStatement psFindById;
    private PreparedStatement psRemove;
    private PreparedStatement psRemoveOrderItem;
    private PreparedStatement psFindOrderItemsByOrderId;

    public PostgresOrderDao(Connection connection, CustomerDao customerDao, ProductDao productDao) {
        super(connection);
        this.customerDao = customerDao;
        this.productDao = productDao;
        try {
            psFindAll = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_ORDER);
            psFindById = connection.prepareStatement("SELECT " + T_COLUMNS + " FROM " + T_ORDER + " WHERE id = ? ");
            psFindOrderItemsByOrderId = connection.prepareStatement(
                    "SELECT " + T_ORDER_ITEM_COLUMNS + " FROM " + T_ORDER_ITEM + " WHERE order_id = ? ");
            psCreate = connection.prepareStatement("INSERT INTO " + T_ORDER + " (" + T_COLUMNS + ") VALUES (DEFAULT, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            psCreateOrderItem = connection.prepareStatement(
                    "INSERT INTO " + T_ORDER_ITEM + " (" + T_ORDER_ITEM_COLUMNS + ") VALUES (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            psRemove = connection.prepareStatement("DELETE FROM " + T_ORDER + " WHERE id = ? ");
            psRemoveOrderItem = connection.prepareStatement("DELETE FROM " + T_ORDER_ITEM + " WHERE order_id = ? ");
        } catch (SQLException e) {
            logger.warning(e.toString());
        }
    }

    @Override
    public Order create(Order order) throws EshopException {
        if (order.getId() != null && order.getId().getId() != null) {
            throw new EshopException("Order id must be null when creating new Order.");
        }
        if (order.getCustomer() == null || order.getCustomer().getId() == null || order.getCustomer().getId().getId() == null) {
            throw new EshopException("Order CUSTOMER ID must not be NULL when creating new Order.");
        }
        try {
            psCreate.setInt(1, order.getCustomer().getId().getId());
            psCreate.executeUpdate();
            Integer orderId = extractGeneratedKey(psCreate);

            for (OrderItem item : order.getItems()) {
                if (item.getProduct() == null || item.getProduct().getId() == null || item.getProduct().getId().getId() == null) {
                    throw new EshopException("Order item PRODUCT ID must not be NULL when creating new OrderItem.");
                }
                psCreateOrderItem.setInt(1, orderId);
                psCreateOrderItem.setInt(2, item.getProduct().getId().getId());
                psCreateOrderItem.setInt(3, item.getCount());
                psCreateOrderItem.executeUpdate();
            }

            commit();
            return findById(new OrderId(orderId));
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
    }

    @Override
    public Order update(Order entity) throws EshopException {
        throw new EshopException("Not implemented yet.");
    }

    @Override
    public void remove(OrderId entityId) throws EshopException {
        try {
            psRemoveOrderItem.setInt(1, entityId.getId());
            psRemoveOrderItem.executeUpdate();
            psRemove.setInt(1, entityId.getId());
            psRemove.executeUpdate();
            commit();
        } catch (SQLException e) {
            rollback();
            throw new EshopException(e);
        }
    }

    @Override
    public List<Order> findAll() throws EshopException {
        try {
            List<Order> orders = new ArrayList<>();
            ResultSet rs = psFindAll.executeQuery();
            while (rs.next()) {
                orders.add(buildOrder(rs));
            }
            return orders;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }


    @Override
    public Order findById(OrderId entityId) throws EshopException {
        try {
            psFindById.setInt(1, entityId.getId());
            ResultSet rs = psFindById.executeQuery();
            if (!rs.next()) {
                return null; // entity not found
            }

            Order Order = buildOrder(rs);

            if (rs.next()) {
                throw new EshopException("Multiple Orders found when searching by id '" + entityId.toString() + "'");
            }

            return Order;
        } catch (SQLException e) {
            throw new EshopException(e);
        }
    }

    protected Order buildOrder(ResultSet rs) throws SQLException, EshopException {
        Integer customerId = rs.getInt("customer_id");
        Integer orderId = rs.getInt("id");

        Customer customer = customerDao.findById(new CustomerId(customerId));

        List<OrderItem> items = findOrderItems(orderId);

        return new Order(new OrderId(orderId), items, customer);
    }

    public List<OrderItem> findOrderItems(Integer orderId) throws SQLException, EshopException {
        List<OrderItem> orderItems = new LinkedList<>();
        psFindOrderItemsByOrderId.setInt(1, orderId);
        ResultSet rs = psFindOrderItemsByOrderId.executeQuery();
        while (rs.next()) {
            orderItems.add(new OrderItem(
                    productDao.findById(new ProductId(rs.getInt("product_id"))),
                    rs.getInt("count")));
        }
        return orderItems;
    }

}
