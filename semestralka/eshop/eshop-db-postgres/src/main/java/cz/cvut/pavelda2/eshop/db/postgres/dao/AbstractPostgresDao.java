package cz.cvut.pavelda2.eshop.db.postgres.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 22. 12. 2015.
 */
public class AbstractPostgresDao {

    protected final Connection connection;

    public AbstractPostgresDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * Extracts the first generated key
     *
     * @param statement
     * @return
     * @throws SQLException
     * @throws EshopException
     */
    protected Integer extractGeneratedKey(PreparedStatement statement) throws SQLException, EshopException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (!generatedKeys.next()) {
            throw new EshopException("No ID generated for statement: " + statement.toString());
        }
        return generatedKeys.getInt(1);
    }


    protected void commit() throws SQLException {
        connection.commit();
    }


    protected void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
