package cz.cvut.pavelda2.eshop.db.postgres.dao;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

import cz.cvut.pavelda2.eshop.db.postgres.PostgresDaoFactory;
import cz.cvut.pavelda2.eshop.integration.dao.impl.OrderDao;
import cz.cvut.pavelda2.eshop.model.entity.*;
import cz.cvut.pavelda2.eshop.utils.exception.EshopException;

/**
 * Created by David Pavel on 3. 12. 2015.
 */
public class PostgresOrderDaoTest {

    static OrderDao orderDao;

    @BeforeClass
    public static void beforeClass() throws EshopException {
        orderDao = new PostgresDaoFactory().getOrderDao();
    }

    @Test
    public void testCreate() throws Exception {
        Order orderToCreate = basicOrder();
        Order order = orderDao.create(orderToCreate);

        System.out.println(order);

        assertNotNull(order);
        assertEquals(orderToCreate.getCustomer(), order.getCustomer());
        assertNotSame(orderToCreate.getCustomer(), order.getCustomer());
        assertThat(order.getItems(), hasSize(3));
        assertThat(orderToCreate.getItems(), containsInAnyOrder(order.getItems().toArray()));
        assertNotSame(orderToCreate.getItems(), order.getItems());
    }

    @Test
    public void testRemove() throws Exception {
        Order order = orderDao.create(basicOrder());
        assertNotNull(order);
        orderDao.remove(order.getId());

        // No order
        Order removedOrder = orderDao.findById(order.getId());
        assertNull(removedOrder);

        // No order items
        List<OrderItem> removedItems = ((PostgresOrderDao) orderDao).findOrderItems(order.getId().getId());
        assertNotNull(removedItems);
        assertThat(removedItems, hasSize(0));

    }

    @Test
    public void testFindById() throws Exception {
        Order order = orderDao.findById(new OrderId(1));
        assertNotNull(order);
        assertEquals("David", order.getCustomer().getName());
        assertThat(order.getItems(), hasSize(3));

    }

    private Order basicOrder() {
        List<OrderItem> items = new ArrayList<>();
        items.add(new OrderItem(new Product(new ProductId(1), "Košík", 129.9), 1));
        items.add(new OrderItem(new Product(new ProductId(2), "Příbory", 350.0), 5));
        items.add(new OrderItem(new Product(new ProductId(3), "Šátek", 300.0), 3));
        return new Order(items, new Customer(new CustomerId(1),"David"));
    }
}