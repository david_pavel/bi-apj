@echo OFF

REM Call script to swtich to JDK 1.8
call jdk18

REM Remove directory with cache 
rd /s /q felix-cache

REM Copy builded modules form project targets 
cp ..\..\semestralka\eshop\eshop-model\target\eshop-model-0.1.jar ..\modules\eshop-model-0.1.jar
cp ..\..\semestralka\eshop\eshop-integration\target\eshop-integration-0.1.jar ..\modules\eshop-integration-0.1.jar
cp ..\..\semestralka\eshop\eshop-localisation\target\eshop-localisation-0.1.jar ..\modules\eshop-localisation-0.1.jar
cp ..\..\semestralka\eshop\eshop-business\target\eshop-business-0.1.jar ..\modules\eshop-business-0.1.jar
cp ..\..\semestralka\eshop\eshop-rich-client\target\eshop-rich-client-0.1.jar ..\modules\eshop-rich-client-0.1.jar
cp ..\..\semestralka\eshop\eshop-utils\target\eshop-utils-0.1.jar ..\modules\eshop-utils-0.1.jar
cp ..\..\semestralka\eshop\eshop-protocol\target\eshop-protocol-0.1.jar ..\modules\eshop-protocol-0.1.jar
cp ..\..\semestralka\eshop\eshop-proxy\target\eshop-proxy-0.1.jar ..\modules\eshop-proxy-0.1.jar
cp ..\..\semestralka\eshop\eshop-connection\target\eshop-connection-0.1.jar ..\modules\eshop-connection-0.1.jar
cp ..\..\semestralka\eshop\eshop-connection-nio\target\eshop-connection-nio-0.1.jar ..\modules\eshop-connection-nio-0.1.jar

REM When adding new module, don't forgot add following file line to felix configuration: 
REM file:..\\modules\\eshop-model-0.1.jar 

REM Run felix 
java -jar felix.jar 

